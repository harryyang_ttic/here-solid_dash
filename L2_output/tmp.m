for l=125:125
    l
    s=acc(l);
    e=acc(l+1);

    fid2=fopen(fullfile(path2,[num2str(s),'_',num2str(e),'_solid_leftb.fuse']),'w');
    for i=s:e
        boundary_name=['chunk_',num2str(i),'*solid_leftb_3D_l2*.fuse'];
        files=dir(fullfile(path2,boundary_name));
        fid=fopen(fullfile(path2,files(1).name));
        A=fscanf(fid,'%f %f %f %*f',[3 inf]);
        fclose(fid);
        A=A';
        mean_A=mean(A);
        fprintf(fid2,'%.15f %.15f %.15f 255\n',mean_A(1),mean_A(2),mean_A(3));
    end
    fclose(fid2);

    fid2=fopen(fullfile(path2,[num2str(s),'_',num2str(e),'_solid_rightb.fuse']),'w');
    for i=s:e
        boundary_name=['chunk_',num2str(i),'*solid_rightb_3D_l2*.fuse'];
        files=dir(fullfile(path2,boundary_name));
        fid=fopen(fullfile(path2,files(1).name));
        A=fscanf(fid,'%f %f %f %*f',[3 inf]);
        fclose(fid);
        A=A';
        mean_A=mean(A);
        fprintf(fid2,'%.15f %.15f %.15f 255\n',mean_A(1),mean_A(2),mean_A(3));
    end
    fclose(fid2);

    fid2=fopen(fullfile(path2,[num2str(s),'_',num2str(e),'_boundary_leftb.fuse']),'w');
    for i=s:e
        boundary_name=['chunk_',num2str(i),'*boundary_leftb_3D_l2*.fuse'];
        files=dir(fullfile(path2,boundary_name));
        fid=fopen(fullfile(path2,files(1).name));
        A=fscanf(fid,'%f %f %f %*f',[3 inf]);
        fclose(fid);
        A=A';
        mean_A=mean(A);
        fprintf(fid2,'%.15f %.15f %.15f 255\n',mean_A(1),mean_A(2),mean_A(3));
    end
    fclose(fid2);

    fid2=fopen(fullfile(path2,[num2str(s),'_',num2str(e),'_boundary_rightb.fuse']),'w');
    for i=s:e
        boundary_name=['chunk_',num2str(i),'*boundary_rightb_3D_l2*.fuse'];
        files=dir(fullfile(path2,boundary_name));
        fid=fopen(fullfile(path2,files(1).name));
        A=fscanf(fid,'%f %f %f %*f',[3 inf]);
        fclose(fid);
        A=A';
        mean_A=mean(A);
        fprintf(fid2,'%.15f %.15f %.15f 255\n',mean_A(1),mean_A(2),mean_A(3));
    end
    fclose(fid2);

    lane_id=1;
    boundary_name=['chunk_',num2str(s),'_*.fuse'];
    files=dir(fullfile(path2,boundary_name));
    fid=fopen(fullfile(path2,files(1).name));
    A=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A=A';
    mean_A=mean(A);

    res=zeros(9000,10);
    for i=s:e-1
        boundary_name=['chunk_',num2str(i),'*dashed_middle_3D_l2*.fuse'];
        files=dir(fullfile(path2,boundary_name));
        p1=cell(1,length(files));
      

        boundary_name2=['chunk_',num2str(i+1),'*dashed_middle_3D_l2*.fuse'];
        files2=dir(fullfile(path2,boundary_name2));
        p2=cell(1,length(files2));
        
        for j=1:length(files)
            if res(i,j)==0
                res(i,j)=lane_id;
                lane_id=lane_id+1;
            end
          end
        
        if size(p1,2)==size(p2,2)
            for j=1:size(p1,2)
                res(i+1,j)=res(i,j);
            end
            continue;
        end
        for j=1:length(files)
            fid=fopen(fullfile(path2,files(j).name));
            A=fscanf(fid,'%f %f %f %*f',[3 inf]);
            fclose(fid);
            A=A';
            [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
            p1{1,j}=[x y z];
        end
        
        for k=1:length(files2)
            fid=fopen(fullfile(path2,files2(k).name));
            A=fscanf(fid,'%f %f %f %*f',[3 inf]);
            fclose(fid);
            A=A';
            [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
            p2{1,k}=[x y z];
        end

      
      
        if size(p1,2)<size(p2,2)
            for j=1:size(p1,2)
                min_dist=10000000;
                for k=1:size(p2,2)
                  
                    a1=p1{1,j};
                    a2=p2{1,k};
                    p=polyfit(a1(:,1),a1(:,2),1);
                    dist=abs(p(1)*a2(:,1)-a2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
                    dist=min(dist);
                   % dist=min(min(pdist2(a1,a2)));
                    if dist<min_dist
                        min_dist=dist;
                        min_k=k;
                    end
                end
                res(i+1,min_k)=res(i,j);
            end
        elseif size(p1,2)>size(p2,2)
            for j=1:size(p2,2)
                min_dist=10000000;
                for k=1:size(p1,2)
                  
                    a1=p2{1,j};
                    a2=p1{1,k};
                    
                    p=polyfit(a1(:,1),a1(:,2),1);
                    dist=abs(p(1)*a2(:,1)-a2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
                    dist=min(dist);
                    
                  %  dist=min(min(pdist2(a1,a2)));
                    if dist<min_dist
                        min_dist=dist;
                        min_k=k;
                    end
                end
                res(i+1,j)=res(i,min_k);
            end
        end
    end

    for j=1:length(files)
        if res(i,j)==0
            res(i,j)=lane_id;
            lane_id=lane_id+1;
        end
    end

    for t=1:lane_id-1
        fid2=fopen(fullfile(path2,[num2str(s),'_',num2str(e),'_dashed_middle_',num2str(t),'.fuse']),'w');
        for i=s:e
            boundary_name=['chunk_',num2str(i),'*dashed_middle_3D_l2*.fuse'];
            files=dir(fullfile(path2,boundary_name));
            p1=cell(1,length(files));
            for j=1:length(files)
                if res(i,j)~=t
                    continue;
                end
                fid=fopen(fullfile(path2,files(j).name));
                A=fscanf(fid,'%f %f %f %*f',[3 inf]);
                fclose(fid);
                A=A';
                mean_A=mean(A);
                fprintf(fid2,'%.15f %.15f %.15f 255\n',mean_A(1),mean_A(2),mean_A(3));
            end
        end
        fclose(fid2);
    end

end
