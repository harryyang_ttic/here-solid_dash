

path='F:\HERE\mergedLanes4_original';
path2='F:\HERE\mergedLanes4_result';
boundary_name='*l2*.fuse';
files=dir(fullfile(path,boundary_name));
for i=1:length(files)
    movefile(fullfile(path,files(i).name),fullfile(path2,files(i).name));
end
    



%% compute distance
res=zeros(endd-start,10);

for i=start:endd-1
    boundary_name=['chunk_',num2str(i),'_*.fuse'];
    files=dir(fullfile(path2,boundary_name));
    p1=cell(1,length(files));
    for j=1:length(files)
        fid=fopen(fullfile(path2,files(j).name));
        A=fscanf(fid,'%f %f %f',[3 inf]);
        fclose(fid);
        A=A';
        [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
        p1{1,j}=[x y z];
    end
    
    boundary_name2=['chunk_',num2str(i+1),'_*.fuse'];
    files2=dir(fullfile(path2,boundary_name2));
    p2=cell(1,length(files2));
    for k=1:length(files2)
        fid=fopen(fullfile(path2,files2(k).name));
        A=fscanf(fid,'%f %f %f',[3 inf]);
        fclose(fid);
        A=A';
        [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
        p2{1,k}=[x y z];
    end
    
    for j=1:size(p1,2)
        for k=1:size(p2,2)
            a1=p1{1,j};
            a2=p2{1,k};
            p=polyfit(a1(:,1),a1(:,2),1);
            dist=abs(p(1)*a2(:,1)-a2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
            if min(dist)<0.3
                res(i,j)=k;
            end
        end
    end
end

for l=1:length(acc)-1
    s=acc(l);
    e=acc(l+1);
    
    for i=s:e-1
        i
        boundary_name=['chunk_',num2str(i),'_*.fuse'];
        files=dir(fullfile(path2,boundary_name));
        p1=cell(1,length(files));
        for j=1:length(files)
            fid=fopen(fullfile(path2,files(j).name));
            A=fscanf(fid,'%f %f %f',[3 inf]);
            fclose(fid);
            A=A';
            [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
            p1{1,j}=[x y z];
        end

        boundary_name2=['chunk_',num2str(i+1),'_*.fuse'];
        files2=dir(fullfile(path2,boundary_name2));
        p2=cell(1,length(files2));
        for k=1:length(files2)
            fid=fopen(fullfile(path2,files2(k).name));
            A=fscanf(fid,'%f %f %f',[3 inf]);
            fclose(fid);
            A=A';
            [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
            p2{1,k}=[x y z];
        end

        for j=1:size(p1,2)
            for k=1:size(p2,2)
                a1=p1{1,j};
                a2=p2{1,k};
                p=polyfit(a1(:,1),a1(:,2),1);
                dist=abs(p(1)*a2(:,1)-a2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
                if min(dist)<0.3
                    res(i,j)=k;
                end
            end
        end
    end

    boundary_name=['chunk_',num2str(i),'_*.fuse'];
end
