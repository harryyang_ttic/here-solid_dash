%res(:,4)=res(:,4)-1;
num=max(res(:,1));
res2=res;
res2(:,7)=res(:,7)+1;
obsData={};
hiddenData={};
for i=1:num
    obsData{i}=res2(res2(:,1)==i,[3:6])';
    hiddenData{i}=res2(res2(:,1)==i,7)';
end
[initState,transmat,mu,Sigma]=gausshmm_train_observed(obsData,hiddenData,2);
Q=length(initState);
mixmat=ones(Q,1);
my_res={};
for i=1:num
    B = mixgauss_prob(obsData{i}, mu, Sigma, mixmat);
    path = viterbi_path(initState, transmat, B); 
    my_res{i}=path;
end

my_res_mat=cell2mat(my_res);
gt_mat=cell2mat(hiddenData);