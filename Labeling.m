res=zeros(endd-start,10);
for i=start:endd-1
    boundary_name=['chunk_',num2str(i),'_*.ned'];
    files=dir(fullfile(path2,boundary_name));
    p1=cell(1,length(files));
    for j=1:length(files)
        fid=fopen(fullfile(path2,files(j).name));
        A=fscanf(fid,'%f %f %f',[3 inf]);
        fclose(fid);
        A=A';
        p1{1,j}=A;
    end
    
    boundary_name2=['chunk_',num2str(i+1),'_*.ned'];
    files2=dir(fullfile(path2,boundary_name2));
    p2=cell(1,length(files2));
    for k=1:length(files2)
        fid=fopen(fullfile(path2,files2(k).name));
        A=fscanf(fid,'%f %f %f',[3 inf]);
        fclose(fid);
        A=A';
        p2{1,k}=A;
    end
    
    for j=1:size(p1,2)
        for k=1:size(p2,2)
            a1=p1{1,j};
            a2=p2{1,k};
            p=polyfit(a1(:,1),a1(:,2),1);
            dist=abs(p(1)*a2(:,1)-a2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
            if min(dist)<0.5
                res(i,j)=k;
            end
        end
    end
end