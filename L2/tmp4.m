for i=1:length(need_interpolate)
        n=need_interpolate(i);
        for j=n-1:-1:start
            if ~ismember(j,need_interpolate)
                break;
            end
        end
        for k=n+1:endd
            if ~ismember(k,need_interpolate)
                break;
            end
        end
        
        filename=['chunk_',num2str(j),'*_boundary_leftb_3D.fuse'];
        files=dir(fullfile(path2,filename));

        f1=textread(fullfile(path2,files(1).name));
        filename=['chunk_',num2str(k),'*_boundary_leftb_3D.fuse'];
        files=dir(fullfile(path2,filename));
        f2=textread(fullfile(path2,files(1).name));
        mean_f1=mean(f1);
        mean_f2=mean(f2);
        f3=f1+repmat((n-j)/(k-j)*(mean_f2-mean_f1),size(f1,1),1);
        
        filename=['chunk_',num2str(n),'*_boundary_leftb_3D.fuse'];
        files=dir(fullfile(path2,filename));
    
    	for t=1:length(files)
            delete(fullfile(path2,files(t).name));
        end
    
        fid3=fopen([path2,'\','chunk_',num2str(n),'_0_0_boundary_leftb_3D.fuse'],'w');
        fprintf(fid3,'%.15f %.15f %.15f %.0f\n',f3');
        fclose(fid3);
        
end