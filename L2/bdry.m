path2=['F:\HERE\bdry_old'];
res3=zeros(0,3);
start=4520;
for i=start+1:endd-1
    filename=['chunk_',num2str(i),'*_boundary_leftb_3D.fuse'];
    files=dir(fullfile(path2,filename));
  
   
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A=A';
    [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A=[x y z];  
    
   
    filename=['chunk_',num2str(i-1),'*_boundary_leftb_3D.fuse'];
    files=dir(fullfile(path2,filename));
 
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A2=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A2=A2';
    [x,y,z]=geodetic2ned(A2(:,1),A2(:,2),A2(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A2=[x y z];
    
    filename=['chunk_',num2str(i+1),'*_boundary_leftb_3D.fuse'];
    
    files=dir(fullfile(path2,filename));
 
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A3=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A3=A3';
    [x,y,z]=geodetic2ned(A3(:,1),A3(:,2),A3(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A3=[x y z];   
    
    p=polyfit(A(:,1),A(:,2),1);
    
    dist=abs(p(1)*A2(:,1)-A2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res3(i-start,1)=i;
    res3(i-start,2)=min(dist);
    
    dist=abs(p(1)*A3(:,1)-A3(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res3(i-start,3)=min(dist);
    
    p=polyfit(A2(:,1),A2(:,2),1);
    dist=abs(p(1)*A3(:,1)-A3(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res3(i-start,4)=min(dist);
end

need_interpolate=[];
for i=1:size(res3,1)
    if res3(i,2)>1 && res3(i,3)>1
        need_interpolate=[need_interpolate;res3(i,1)];
    end
end

for i=1:length(need_interpolate)
        n=need_interpolate(i);
        for j=n-1:-1:start
            if ~ismember(need_interpolate)
                break;
            end
        end
        for k=n+1:endd
            if ~ismember(need_interpolate)
                break;
            end
        end
        
        filename=['chunk_',num2str(j),'*_boundary_leftb_3D.fuse'];
        files=dir(fullfile(path2,filename));

        f1=textread(files(1).name);
        filename=['chunk_',num2str(k),'*_boundary_leftb_3D.fuse'];
        files=dir(fullfile(path2,filename));
        f2=textread(files(1).name);
        mean_f1=mean(f1);
        mean_f2=mean(f2);
        f3=f1+repmat((n-j)/(k-j)*(mean_f2-mean_f1),size(f1,1),1);
        
        fid3=fopen([path2,'\','chunk_',num2str(n),'_0_0_boundary_rightb_3D_interpolated.fuse'],'w');
        fprintf(fid3,'%.15f %.15f %.15f %.0f\n',f3');
        fclose(fid3);
        
end

for i=start+1:endd-1
    filename=['chunk_',num2str(i),'*_boundary_leftb_3D.fuse'];
    files=dir(fullfile(path2,filename));
    
    if length(files)>1
        filename=['chunk_',num2str(i),'*_2_boundary_leftb_3D.fuse'];
        delete(filename);
    end
end
    