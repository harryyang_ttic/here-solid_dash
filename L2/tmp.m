path='E:\marking\6944320-mergedLanes';
path2='F:HERE\mergedLanes1_chucai_L2';

%sorted2=sortrows(sorted2,[2,3]);

for i=start:endd
    boundary_name=['chunk_',num2str(i),'_0_*.fuse'];
    files=dir(fullfile(path,boundary_name));
  %  ids=sorted2(sorted2(:,2)==i,6);
    tmp=0;
    row=find(R_chucai(:,1)==i);
    if isempty(row)
        continue;
    end
    for j=1:length(files)
        id=R(row,j+2);
        if id==0
            continue;
            %{
            name=['chunk_',num2str(i),'_',num2str(j),'_unknown_3D.fuse'];
            copyfile(fullfile(path,files(j).name),fullfile(path2,name));
            %}
        elseif id==1 && tmp==0
            name=['chunk_',num2str(i),'_',num2str(j),'_solid_leftb_3D.fuse'];
            tmp=1;
            if exist(fullfile(path2,name))
                continue;
            end
            copyfile(fullfile(path,files(j).name),fullfile(path2,name));
        elseif id==1 && tmp>0
            name=['chunk_',num2str(i),'_',num2str(j),'_solid_rightb_3D.fuse'];
            if exist(fullfile(path2,name))
                continue;
            end
            copyfile(fullfile(path,files(j).name),fullfile(path2,name));
        elseif id==2
            name=['chunk_',num2str(i),'_',num2str(j),'_dashed_middle_3D.fuse'];
            tmp=1;
            if exist(fullfile(path2,name))
                continue;
            end
            copyfile(fullfile(path,files(j).name),fullfile(path2,name));
            
        end
            
    end
end

%% left solid
need_interpolate=[];
for i=start:endd
    ids=R_chucai(R_chucai(:,1)==i,:);
    ids_1=find(ids==1);
    if isempty(ids_1)
        need_interpolate=[need_interpolate;i];
    else
        ids_2=find(ids==2);
        if ~isempty(ids_2) && ids_2(1)<ids_1(1)
            need_interpolate=[need_interpolate;i];
        end
    end
end

%% interpolate
for i=1:length(need_interpolate)
    for j=i-1:start
        ids=R_chucai(R_chucai(:,1)==i,:);
        ids_1=find(ids==1);
        if ~isempty(ids_1)
            ids_2=find(ids==2);
            if ~isempty(ids_2) && ids_2(1)>ids_1(1)
                break;
            end
        end
    end
    f1=textread('F:\HERE\mergedLanes1_all4\chunk_',num2str(j),'_0_',num2str(ids_1(1)),'_solid_leftb_3D.fuse');
    f3=f1+(i-j)*(repmat(f1(end,:),size(f1,1),1)-repmat(f1(1,:),size(f1,1),1));
    fid3=fopen('F:\HERE\mergedLanes1_all4\chunk_',num2str(i),'_0_0_solid_leftb_3D.fuse');
    fprintf(fid3,'%.15f %.15f %.15f %.0f\n',f3');
    fclose(fid3);

end
