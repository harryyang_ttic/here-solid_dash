for i=start+1:endd-1
    filename=['chunk_',num2str(i),'*_solid_leftb_3D.fuse'];
    files=dir(fullfile(path2,filename));
 
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A=A';
    [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A=[x y z];   
   
    filename=['chunk_',num2str(i-1),'*_solid_leftb_3D.fuse'];
    files=dir(fullfile(path2,filename));
 
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A2=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A2=A2';
    
    [x,y,z]=geodetic2ned(A2(:,1),A2(:,2),A2(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A2=[x y z];   
   
    filename=['chunk_',num2str(i+1),'*_solid_leftb_3D.fuse'];
    files=dir(fullfile(path2,filename));
 
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A3=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A3=A3';
    [x,y,z]=geodetic2ned(A3(:,1),A3(:,2),A3(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A3=[x y z];   
    
    p=polyfit(A(:,1),A(:,2),1);
    
    dist=abs(p(1)*A2(:,1)-A2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res(i-start,1)=i;
    res(i-start,2)=min(dist);
    
    dist=abs(p(1)*A3(:,1)-A3(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res(i-start,3)=min(dist);
    
    p=polyfit(A2(:,1),A2(:,2),1);
    dist=abs(p(1)*A3(:,1)-A3(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res(i-start,4)=min(dist);
end

need_check=[];
for i=1:size(res,1)
    if res(i,2)>0.3 || res(i,3)>0.3
        need_check=[need_check;res(i,:)];
    end
end

need_interpolate=[];
i=1;
while i<=size(need_check,1)
    for j=i:size(need_check,1)+1
        if j>size(need_check,1) || need_check(j,1)-need_check(i,1)>j-i
            break;
        end
    end
    if j-i>=3
        for j=i+1:j-2
            need_interpolate=[need_interpolate;need_check(j,1)];
        end
    end
    i=j;
end

for i=1:length(need_interpolate)
    filename=['chunk_',num2str(need_interpolate(i)),'*_solid_leftb_3D.fuse'];
    files=dir(fullfile(path2,filename));
    movefile(fullfile(path2,files(1).name),fullfile(path2,'old',files(1).name));
end


for i=1:length(need_interpolate)
    n=need_interpolate(i);
    for j1=n-1:-1:start
        filename=['chunk_',num2str(j1),'*_solid_leftb_3D.fuse'];
        files1=dir(fullfile(path2,filename));
        if length(files1)>0
            break;
        end
    end
    for j2=n+1:endd
        filename=['chunk_',num2str(j2),'*_solid_leftb_3D.fuse'];
        files2=dir(fullfile(path2,filename));
        if length(files2)>0
            break;
        end
    end
    f1=textread(fullfile(path2,files1(1).name));
    f2=textread(fullfile(path2,files2(1).name));
    mean_f1=mean(f1);
    mean_f2=mean(f2);
    f3=f1+repmat((n-j1)/(j2-j1)*(mean_f2-mean_f1),size(f1,1),1);
   % f3=f1+(n-j)*(repmat(f1(end,:),size(f1,1),1)-repmat(f1(1,:),size(f1,1),1));
    fid3=fopen([my_path,'\chunk_',num2str(n),'_1_solid_leftb_3D.fuse'],'w');
    fprintf(fid3,'%.15f %.15f %.15f %.0f\n',f3');
    fclose(fid3);
end



for i=start+1:endd-1
    filename=['chunk_',num2str(i),'*_solid_rightb_3D.fuse'];
    files=dir(fullfile(path2,filename));
 
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A=A';
    [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A=[x y z];   
   
    filename=['chunk_',num2str(i-1),'*_solid_rightb_3D.fuse'];
    files=dir(fullfile(path2,filename));
 
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A2=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A2=A2';
    
    [x,y,z]=geodetic2ned(A2(:,1),A2(:,2),A2(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A2=[x y z];   
   
    filename=['chunk_',num2str(i+1),'*_solid_rightb_3D.fuse'];
    files=dir(fullfile(path2,filename));
 
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A3=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A3=A3';
    [x,y,z]=geodetic2ned(A3(:,1),A3(:,2),A3(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A3=[x y z];   
    
    p=polyfit(A(:,1),A(:,2),1);
    
    dist=abs(p(1)*A2(:,1)-A2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res(i-start,1)=i;
    res(i-start,2)=min(dist);
    
    dist=abs(p(1)*A3(:,1)-A3(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res(i-start,3)=min(dist);
    
    p=polyfit(A2(:,1),A2(:,2),1);
    dist=abs(p(1)*A3(:,1)-A3(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res(i-start,4)=min(dist);
end

need_check=[];
for i=1:size(res,1)
    if res(i,2)>0.3 || res(i,3)>0.3
        need_check=[need_check;res(i,:)];
    end
end

need_interpolate=[];
i=1;
while i<=size(need_check,1)
    for j=i:size(need_check,1)+1
        if j>size(need_check,1) || need_check(j,1)-need_check(i,1)>j-i
            break;
        end
    end
    if j-i>=3
        for j=i+1:j-2
            need_interpolate=[need_interpolate;need_check(j,1)];
        end
    end
    i=j;
end

for i=1:length(need_interpolate)
    filename=['chunk_',num2str(need_interpolate(i)),'*_solid_rightb_3D.fuse'];
    files=dir(fullfile(path2,filename));
    movefile(fullfile(path2,files(1).name),fullfile(path2,'old',files(1).name));
end


for i=1:length(need_interpolate)
    n=need_interpolate(i);
    for j1=n-1:-1:start
        filename=['chunk_',num2str(j1),'*_solid_rightb_3D.fuse'];
        files1=dir(fullfile(path2,filename));
        if length(files1)>0
            break;
        end
    end
    for j2=n+1:endd
        filename=['chunk_',num2str(j2),'*_solid_rightb_3D.fuse'];
        files2=dir(fullfile(path2,filename));
        if length(files2)>0
            break;
        end
    end
    f1=textread(fullfile(path2,files1(1).name));
    f2=textread(fullfile(path2,files2(1).name));
    mean_f1=mean(f1);
    mean_f2=mean(f2);
    f3=f1+repmat((n-j1)/(j2-j1)*(mean_f2-mean_f1),size(f1,1),1);
   % f3=f1+(n-j)*(repmat(f1(end,:),size(f1,1),1)-repmat(f1(1,:),size(f1,1),1));
    fid3=fopen([my_path,'\chunk_',num2str(n),'_1_solid_rightb_3D.fuse'],'w');
    fprintf(fid3,'%.15f %.15f %.15f %.0f\n',f3');
    fclose(fid3);
end

res=[];
for i=start+1:endd-1
    filename=['chunk_',num2str(i),'*_dashed_middle*.fuse'];
    ofiles=dir(fullfile(path2,filename));
    for j=1:length(ofiles)
        boundary_out_fullname=fullfile(path2,ofiles(j).name);
        fid=fopen(boundary_out_fullname);
        A=fscanf(fid,'%f %f %f %*f',[3 inf]);
        fclose(fid);
        A=A';
        [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
        A=[x y z];  
        p=polyfit(A(:,1),A(:,2),1);

        filename=['chunk_',num2str(i-1),'*_dashed_middle*.fuse'];
        files=dir(fullfile(path2,filename));
        min_dist_pre=10000;
        for k=1:length(files)
            boundary_out_fullname=fullfile(path2,files(k).name);
            fid=fopen(boundary_out_fullname);
            A2=fscanf(fid,'%f %f %f %*f',[3 inf]);
            fclose(fid);
            A2=A2';
            [x,y,z]=geodetic2ned(A2(:,1),A2(:,2),A2(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
            A2=[x y z];
            dist=abs(p(1)*A2(:,1)-A2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
            min_dist=min(dist);
            if min_dist<min_dist_pre
                min_dist_pre=min_dist;
            end
        end
        
        filename=['chunk_',num2str(i+1),'*_dashed_middle*.fuse'];
        files=dir(fullfile(path2,filename));
         min_dist_post=10000;
         for k=1:length(files)
            boundary_out_fullname=fullfile(path2,files(k).name);
            fid=fopen(boundary_out_fullname);
            A2=fscanf(fid,'%f %f %f %*f',[3 inf]);
            fclose(fid);
            A2=A2';
            [x,y,z]=geodetic2ned(A2(:,1),A2(:,2),A2(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
            A2=[x y z];
            dist=abs(p(1)*A2(:,1)-A2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
            min_dist=min(dist);
            if min_dist<min_dist_post
                min_dist_post=min_dist;
            end
        end
         
        res(i-start,1)=i;
        res(i-start,2*j)=min_dist_pre;
        res(i-start,2*j+1)=min_dist_post;
        
    end
end

for i=1:size(res,1)
    filename=['chunk_',num2str(res(i,1)),'*_dashed_middle*.fuse'];
    ofiles=dir(fullfile(path2,filename));
    id=1;
    for j=2:2:8
        if res(i,j)>.3 && res(i,j+1)>.3
            movefile(fullfile(path2,ofiles(id).name),fullfile(path2,'old',ofiles(id).name));
        end
        id=id+1;
    end
end

need_interpolate=[];
for i=1:size(res,1)
    filename=['chunk_',num2str(res(i,1)),'*_dashed_middle*.fuse'];
    ofiles=dir(fullfile(path2,filename));
    id=1;
    flag=false;
    for j=2:2:8
        if res(i,j)>.3 && res(i,j+1)>.3
           flag=true;
        end
        
    end
    if flag
        need_interpolate=[need_interpolate;res(i,1)];
    end
end


for i=1:length(need_interpolate)
    n=need_interpolate(i);
    for j1=n-1:-1:start
        if ~ismember(j1,need_interpolate)
            break;
        end
    end
    for j2=n+1:endd
        if ~ismember(j2,need_interpolate)
            break;
        end
    end
    
    filename=['chunk_',num2str(j1),'*_dashed_middle*.fuse'];
    files1=dir(fullfile(path2,filename));
    
    filename=['chunk_',num2str(j2),'*_dashed_middle*.fuse'];
    files2=dir(fullfile(path2,filename));
    
    filename=['chunk_',num2str(n),'*_dashed_middle*.fuse'];
    files3=dir(fullfile(path2,filename));
    
    for j=1:min(length(files1),length(files2))
        f1=textread(fullfile(path2,files1(j).name));
        f2=textread(fullfile(path2,files2(j).name));
        mean_f1=mean(f1);
        mean_f2=mean(f2);
        f3=f1+repmat((n-j1)/(j2-j1)*(mean_f2-mean_f1),size(f1,1),1);
        
        p=polyfit(f3(:,1),f3(:,2),1);
        flag=false;
        
        for k=1:length(files3)
            A2=textread(fullfile(path2,files3(k).name));
            dist=abs(p(1)*A2(:,1)-A2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
            if min(dist)<.3
                flag=true;
            end
        end
        if flag
            continue;
        end
       % f3=f1+(n-j)*(repmat(f1(end,:),size(f1,1),1)-repmat(f1(1,:),size(f1,1),1));
        fid3=fopen([my_path,'\chunk_',num2str(n),'_',num2str(j),'_dashed_middle_3D.fuse'],'w');
        fprintf(fid3,'%.15f %.15f %.15f %.0f\n',f3');
        fclose(fid3);
    end
    
end