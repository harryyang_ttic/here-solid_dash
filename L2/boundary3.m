LR='leftb';

res=[];
for i=start:endd
    filename=['chunk_',num2str(i),'*_boundary_',LR,'_3D.fuse'];
    files=dir(fullfile(path3,filename));
 
    boundary_out_fullname=fullfile(path3,files(1).name);
    fid=fopen(boundary_out_fullname);
    A=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A=A';
    [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A=[x y z];   
   
    filename=['chunk_',num2str(i),'*_pose.fuse'];
    files=dir(fullfile('F:\HERE\pose',filename));
 
    boundary_out_fullname=fullfile('F:\HERE\pose',files(1).name);
    fid=fopen(boundary_out_fullname);
    A2=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A2=A2';
    [x,y,z]=geodetic2ned(A2(:,1),A2(:,2),A2(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A2=[x y z];   
      
    p=polyfit(A2(:,1),A2(:,2),1);
    
    dist=abs(p(1)*A(:,1)-A(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res(i-start+1,1)=i;
    res(i-start+1,2)=mean(dist);
 
end

%% get pieces
id=1;
res(1,3)=id;
for i=start+1:endd
    if abs(res(i-start+1,2)-res(i-start,2))>1
        id=id+1;
    end
    res(i-start+1,3)=id;
end

%% fit pieces

for i=1:max(res(:,3))
    this_res=res(res(:,3)==i,:);
    if size(this_res,1)<=2
        continue;
    end
    start_id=min(this_res(:,1));
    end_id=max(this_res(:,1));
    A_all=[];
    
    breaks=[];
    for j=start_id:end_id
        filename=['chunk_',num2str(j),'*_boundary_',LR,'_3D.fuse'];
        files=dir(fullfile(path3,filename));

        boundary_out_fullname=fullfile(path3,files(1).name);
        fid=fopen(boundary_out_fullname);
        A=fscanf(fid,'%f %f %f %*f',[3 inf]);
        fclose(fid);
        A=A';
        [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
        A=[x y z];
        A(:,4)=j;
        A_all=[A_all;A];
        if j==start_id
            breaks=[breaks;A(1,1)];
        end
        if mod(j-start_id,5)==0 || j==end_id
            breaks=[breaks;A(end,1)];
        end
    end
    
    pp2=splinefit(A_all(:,1),A_all(:,2),breaks,3);
  %  t2=ppval(pp2,A_all(:,1));
    A4=A_all;
 %   A4(:,2)=t2;
  
    for j=start_id:end_id
        this_A=A4(A4(:,4)==j,:);
        first_pt=this_A(1,1:3);
        last_pt=this_A(end,1:3);
        new_A(1,1:3)=first_pt;
        for k=1:5000
            cur_pt=first_pt+k/5000*(last_pt-first_pt);
            new_A(k+1,1:3)=cur_pt;
        end
        new_A(:,2)=ppval(pp2,new_A(:,1));
        this_A=new_A;
        [lat,lon,h]=ned2geodetic(this_A(:,1),this_A(:,2),this_A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
        fid=fopen(fullfile(path3,'new',['chunk_',num2str(j),'_0_0_boundary_',LR,'_3D.fuse']),'w');
        fprintf(fid,'%.15f %.15f %.15f 255\n',[lat,lon,h]');
        fclose(fid);
    end
end


%% interpolate
for i=1:max(res(:,3))
    this_res=res(res(:,3)==i,:);
    if size(this_res,1)>2 || i==1 || i==max(res(:,3))
        continue;
    end
   
    start_id=min(this_res(:,1));
    end_id=max(this_res(:,1));
    
    for j=start_id:end_id
        for j1=j-1:-1:start
            filename=['chunk_',num2str(j1),'_0_0_boundary_',LR,'_3D.fuse'];
            files=dir(fullfile(path3,'new',filename));
            if ~isempty(files) 
                this_id=res(res(:,1)==j1,3);
                if sum(res(:,3)==this_id)<=2
                    continue;
                end
                
                boundary_out_fullname=fullfile(path3,'new',files(1).name);
                fid=fopen(boundary_out_fullname);
                A=fscanf(fid,'%f %f %f %*f',[3 inf]);
                fclose(fid);
                A=A';
                [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
                A1=[x y z];
                break;
            end   
        end

        for j2=j+1:endd
            filename=['chunk_',num2str(j2),'_0_0_boundary_',LR,'_3D.fuse'];
            files=dir(fullfile(path3,'new',filename));
            if ~isempty(files)   
                this_id=res(res(:,1)==j2,3);
                if sum(res(:,3)==this_id)<=2
                    continue;
                end
                
                boundary_out_fullname=fullfile(path3,'new',files(1).name);
                fid=fopen(boundary_out_fullname);
                A=fscanf(fid,'%f %f %f %*f',[3 inf]);
                fclose(fid);
                A=A';
                [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
                A2=[x y z];
                break;
            end   
        end

        %% interpolate
        first_pt=A1(end,:);
        last_pt=A2(1,:);

        first_pt1=first_pt+(last_pt-first_pt)*(j-j1-1)/(j2-j1-1);
        last_pt=first_pt1+1/(j2-j1-1)*(last_pt-first_pt);
        new_A(1,1:3)=first_pt1;
        for k=1:5000
            cur_pt=first_pt1+k/5000*(last_pt-first_pt1);
            new_A(k+1,1:3)=cur_pt;
        end

         this_A=new_A;
        [lat,lon,h]=ned2geodetic(this_A(:,1),this_A(:,2),this_A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
        fid=fopen(fullfile(path3,'new',['chunk_',num2str(j),'_0_0_boundary_',LR,'_3D.fuse']),'w');
        fprintf(fid,'%.15f %.15f %.15f 255\n',[lat,lon,h]');
        fclose(fid);
    end
end