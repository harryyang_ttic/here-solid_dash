acc_folder='F:\HERE\6944320_acc_chucai';
acc_list=textread(fullfile(acc_folder,'6944320_accessor_chunk_list.csv'));
path2='F:\HERE\mergedLanes1_all';

D_all=zeros(0,10);
res=zeros(0,10);
%for i=1:length(acc_list)-1
for i=1:length(acc_list)-1
    i
    fid=fopen(fullfile(acc_folder,[num2str(acc_list(i)),'.fuse']));
    A=fscanf(fid,'%f,%f,%f,%*f\n');
    fclose(fid);
    A=reshape(A,3,[]);
    A=A';
    
    start=acc_list(i);

    [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    M=[x,y,z];
    
    boundary_name=['chunk_',num2str(start),'_*.ned'];
    files=dir(fullfile(path2,boundary_name));
  
    p1=cell(1,length(files));
   
    for k=1:length(files)
        boundary_out_fullname=fullfile(path2,files(k).name);
        fid=fopen(boundary_out_fullname);
        A=fscanf(fid,'%f %f %f',[3 inf]);
        fclose(fid);
        A=A';
        p1{1,k}=A;
    end
    
   D=zeros(length(files),size(M,1)-2);
   for j=1:size(p1,2)
        a1=p1{1,j};
        p=polyfit(a1(:,1),a1(:,2),1);
        for k=2:size(M,1)-1
            dist=abs(p(1)*M(k,1)-M(k,2)+p(2))/sqrt(p(1)*p(1)+1);
            D(j,k-1)=dist;
        end
   end
   D_all(i,1:size(D,2))=min(D);
   
   endd=acc_list(i+1);
   
   %% left solid
   cur_id=find(D(:,1)==D_all(i,1));
   res(start,cur_id)=1;
   a1=p1{1,cur_id};
   for j=start+1:endd
        p=polyfit(a1(:,1),a1(:,2),1);
        boundary_name2=['chunk_',num2str(j),'_*.ned'];
        files2=dir(fullfile(path2,boundary_name2));
        MIN_DIST=10000;
        this_id=-1;
        for k=1:length(files2)
            fid=fopen(fullfile(path2,files2(k).name));
            A=fscanf(fid,'%f %f %f',[3 inf]);
            fclose(fid);
            A=A';
            dist=abs(p(1)*A(:,1)-A(:,2)+p(2))/sqrt(p(1)*p(1)+1);
            if min(dist)<MIN_DIST
                this_id=k;
                MIN_DIST=dist;
                a1=A;
            end
        end
        if this_id>0
            res(j,this_id)=1;
        end
   end
   
   %% right solid
   cur_id=find(D(:,end)==D_all(i,size(M,1)-2));
   res(start,cur_id)=1;
   a1=p1{1,cur_id};
   for j=start+1:endd
        p=polyfit(a1(:,1),a1(:,2),1);
        boundary_name2=['chunk_',num2str(j),'_*.ned'];
        files2=dir(fullfile(path2,boundary_name2));
        MIN_DIST=10000;
        this_id=-1;
        for k=1:length(files2)
            fid=fopen(fullfile(path2,files2(k).name));
            A=fscanf(fid,'%f %f %f',[3 inf]);
            fclose(fid);
            A=A';
            dist=abs(p(1)*A(:,1)-A(:,2)+p(2))/sqrt(p(1)*p(1)+1);
            if min(dist)<MIN_DIST
                this_id=k;
                MIN_DIST=dist;
                a1=A;
            end
        end
        if this_id>0
            res(j,this_id)=1;
        end
   end
   
   %% do it again?
   if i==1
       continue;
   end
   cur_id=find(D(:,1)==D_all(i,1));
   res(start,cur_id)=1;
   a1=p1{1,cur_id};
   endd=acc_list(i-1);
   for j=start-1:-1:endd
        p=polyfit(a1(:,1),a1(:,2),1);
        boundary_name2=['chunk_',num2str(j),'_*.ned'];
        files2=dir(fullfile(path2,boundary_name2));
         MIN_DIST=10000;
        this_id=-1;
        for k=1:length(files2)
            fid=fopen(fullfile(path2,files2(k).name));
            A=fscanf(fid,'%f %f %f',[3 inf]);
            fclose(fid);
            A=A';
             dist=abs(p(1)*A(:,1)-A(:,2)+p(2))/sqrt(p(1)*p(1)+1);
             if min(dist)<MIN_DIST
                this_id=k;
                MIN_DIST=dist;
                a1=A;
            end
        end
        if this_id>0
            res(j,this_id)=1;
        end
   end
   
   cur_id=find(D(:,end)==D_all(i,size(M,1)-2));
   res(start,cur_id)=1;
   a1=p1{1,cur_id};
   for j=start-1:-1:endd
        p=polyfit(a1(:,1),a1(:,2),1);
        boundary_name2=['chunk_',num2str(j),'_*.ned'];
        files2=dir(fullfile(path2,boundary_name2));
        MIN_DIST=10000;
        this_id=-1;
        for k=1:length(files2)
            fid=fopen(fullfile(path2,files2(k).name));
            A=fscanf(fid,'%f %f %f',[3 inf]);
            fclose(fid);
            A=A';
            dist=abs(p(1)*A(:,1)-A(:,2)+p(2))/sqrt(p(1)*p(1)+1);
            if min(dist)<MIN_DIST
                this_id=k;
                MIN_DIST=dist;
                a1=A;
            end
        end
        if this_id>0
            res(j,this_id)=1;
        end
   end
   
end

for i=1:size(res,1)
    t1=find(res(i,:)==1);
    for j=min(t1)+1:max(t1)-1
        res(i,j)=2;
    end
end
