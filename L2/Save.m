start=1;
endd=8500;

path='E:\marking\6944320-mergedLanes';
path2='F:HERE\mergedLanes1_all5';

%sorted2=sortrows(sorted2,[2,3]);

for i=start:endd
    boundary_name=['chunk_',num2str(i),'_0_*.fuse'];
    files=dir(fullfile(path,boundary_name));
  %  ids=sorted2(sorted2(:,2)==i,6);
    tmp=0;  
    for j=1:length(files)
        id=res(i,j);
        if id==0
            continue;
            %{
            name=['chunk_',num2str(i),'_',num2str(j),'_unknown_3D.fuse'];
            copyfile(fullfile(path,files(j).name),fullfile(path2,name));
            %}
        elseif id==1 && tmp==0
            name=['chunk_',num2str(i),'_',num2str(j),'_solid_leftb_3D.fuse'];
            tmp=1;
            if exist(fullfile(path2,name))
                continue;
            end
            copyfile(fullfile(path,files(j).name),fullfile(path2,name));
        elseif id==1 && tmp>0
            name=['chunk_',num2str(i),'_',num2str(j),'_solid_rightb_3D.fuse'];
            if exist(fullfile(path2,name))
                continue;
            end
            copyfile(fullfile(path,files(j).name),fullfile(path2,name));
        elseif id==2
            name=['chunk_',num2str(i),'_',num2str(j),'_dashed_middle_3D.fuse'];
            tmp=1;
            if exist(fullfile(path2,name))
                continue;
            end
            copyfile(fullfile(path,files(j).name),fullfile(path2,name));
            
        end
            
    end
end