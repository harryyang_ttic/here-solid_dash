%% step 1: fill in mising marks

my_path='F:\HERE\mergedLanes4_original';

need_interpolate=[];
for i=start:endd
    ids=R_chucai(R_chucai(:,1)==i,:);
    ids_1=find(ids==1);
    if isempty(ids_1)
        need_interpolate=[need_interpolate;i];
    else
        ids_2=find(ids==2);
        if ~isempty(ids_2) && ids_2(1)<ids_1(1)
            need_interpolate=[need_interpolate;i];
        end
        if isempty(ids_2)
            files=dir([my_path,'\chunk_',num2str(i),'*_solid_left*']);
            if length(files)==0
                need_interpolate=[need_interpolate;i];
            end
        end
    end
end

for i=1:length(need_interpolate)
    n=need_interpolate(i);
    for j1=n-1:-1:start
        ids=R_chucai(R_chucai(:,1)==j1,:);
        ids_11=find(ids==1);
        if ~isempty(ids_11)
            ids_2=find(ids==2);
            if ~isempty(ids_2) && ids_2(1)>ids_11(1)
                break;
            end
        end
    end
    for j2=n+1:endd
        ids=R_chucai(R_chucai(:,1)==j2,:);
        ids_12=find(ids==1);
        if ~isempty(ids_12)
            ids_2=find(ids==2);
            if ~isempty(ids_2) && ids_2(1)>ids_12(1)
                break;
            end
        end
    end
    f1=textread([my_path,'\chunk_',num2str(j1),'_',num2str(ids_11(1)-1),'_solid_leftb_3D.fuse']);
    f2=textread([my_path,'\chunk_',num2str(j2),'_',num2str(ids_12(1)-1),'_solid_leftb_3D.fuse']);
    mean_f1=mean(f1);
    mean_f2=mean(f2);
    f3=f1+repmat((n-j1)/(j2-j1)*(mean_f2-mean_f1),size(f1,1),1);
   % f3=f1+(n-j)*(repmat(f1(end,:),size(f1,1),1)-repmat(f1(1,:),size(f1,1),1));
    fid3=fopen([my_path,'\chunk_',num2str(n),'_1_solid_leftb_3D.fuse'],'w');
    fprintf(fid3,'%.15f %.15f %.15f %.0f\n',f3');
    fclose(fid3);

end


need_interpolate=[];
for i=start:endd
    ids=R_chucai(R_chucai(:,1)==i,:);
    ids_1=find(ids==1);
    if isempty(ids_1)
        need_interpolate=[need_interpolate;i];
    else
        ids_2=find(ids==2);
        if ~isempty(ids_2) && ids_2(1)>ids_1(end)
            need_interpolate=[need_interpolate;i];
        end
        if isempty(ids_2)
            files=dir([my_path,'\chunk_',num2str(i),'*_solid_right*']);
            if length(files)==0
                i
                need_interpolate=[need_interpolate;i];
            end
        end
    end
end

for i=1:length(need_interpolate)
    n=need_interpolate(i);
    for j1=n-1:-1:start
        ids=R_chucai(R_chucai(:,1)==j1,:);
        ids_11=find(ids==1);
        if ~isempty(ids_11)
            ids_2=find(ids==2);
            if ~isempty(ids_2) && ids_2(1)<ids_11(end)
                break;
            end
        end
    end
    for j2=n+1:endd
        ids=R_chucai(R_chucai(:,1)==j2,:);
        ids_12=find(ids==1);
        if ~isempty(ids_12)
            ids_2=find(ids==2);
            if ~isempty(ids_2) && ids_2(1)<ids_12(end)
                break;
            end
        end
    end
    f1=textread([my_path,'\chunk_',num2str(j1),'_',num2str(ids_11(end)-1),'_solid_rightb_3D.fuse']);
    f2=textread([my_path,'\chunk_',num2str(j2),'_',num2str(ids_12(end)-1),'_solid_rightb_3D.fuse']);
    mean_f1=mean(f1);
    mean_f2=mean(f2);
    f3=f1+repmat((n-j1)/(j2-j1)*(mean_f2-mean_f1),size(f1,1),1);
   % f3=f1+(n-j)*(repmat(f1(end,:),size(f1,1),1)-repmat(f1(1,:),size(f1,1),1));
    fid3=fopen([my_path,'\chunk_',num2str(n),'_4_solid_rightb_3D.fuse'],'w');
    fprintf(fid3,'%.15f %.15f %.15f %.0f\n',f3');
    fclose(fid3);

end

need_interpolate=[];
for i=start:endd
    ids=R_chucai(R_chucai(:,1)==i,:);
    ids_1=find(ids==2);
    if isempty(ids_1)
        need_interpolate=[need_interpolate;i];
    end
end

for i=1:length(need_interpolate)
    n=need_interpolate(i);
    for j1=n-1:-1:start
        ids=R_chucai(R_chucai(:,1)==j1,:);
        ids_11=find(ids==2);
        if ~isempty(ids_11)
           break;
        end
    end
    for j2=n+1:endd
        ids=R_chucai(R_chucai(:,1)==j2,:);
        ids_12=find(ids==2);
        if ~isempty(ids_12)
            break;
        end
    end
    for j=1:min(length(ids_11),length(ids_12))
        f1=textread([my_path,'\chunk_',num2str(j1),'_',num2str(ids_11(j)-1),'_dashed_middle_3D.fuse']);
        f2=textread([my_path,'\chunk_',num2str(j2),'_',num2str(ids_12(j)-1),'_dashed_middle_3D.fuse']);
        mean_f1=mean(f1);
        mean_f2=mean(f2);
        f3=f1+repmat((n-j1)/(j2-j1)*(mean_f2-mean_f1),size(f1,1),1);
       % f3=f1+(n-j)*(repmat(f1(end,:),size(f1,1),1)-repmat(f1(1,:),size(f1,1),1));
        fid3=fopen([my_path,'\chunk_',num2str(n),'_',num2str(j),'_dashed_middle_3D.fuse'],'w');
        fprintf(fid3,'%.15f %.15f %.15f %.0f\n',f3');
        fclose(fid3);

    end
    
end

%% reject outlier



