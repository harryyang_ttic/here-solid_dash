path2='F:\HERE\bdry_old';
res3=zeros(0,3);
start=4520;

A_all=[];
breaks=[];
for i=start:endd-1
    filename=['chunk_',num2str(i),'*_boundary_leftb_3D.fuse'];
    files=dir(fullfile(path2,filename));
  
   
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A=A';
    [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A=[x y z];  
    A_all=[A_all;A];
    if mod(i-start,4)==0
        breaks=[breaks;A(end,1)];
    end
end

%pp2=splinefit(A_all(:,1),A_all(:,2),8,5);
pp2=splinefit(A_all(:,1),A_all(:,2),breaks,3);
t2=ppval(pp2,A_all(:,1));
A4=A_all;
A4(:,2)=t2;
[lat,lon,h]=ned2geodetic(A4(:,1),A4(:,2),A4(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
fid=fopen('res_left.txt','w');
fprintf(fid,'%.15f %.15f %.15f 255\n',[lat,lon,h]');
fclose(fid);