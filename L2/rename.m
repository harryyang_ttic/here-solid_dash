path4='F:\HERE\L2_result';

for i=start:endd
    id=0;
    filename=['chunk_',num2str(i),'_*boundary_leftb*.fuse'];
    files=dir(fullfile(path3,filename));
    a2=fullfile(path4,['chunk_',num2str(i),'_0_',num2str(id),'_boundary_leftb_3D.fuse']);
    id=id+1;
    copyfile(fullfile(path3,files(1).name),a2);
    
    filename=['chunk_',num2str(i),'_*solid_leftb_3D.fuse'];
    files=dir(fullfile(path2,filename));
    for j=1:length(files)
        a2=fullfile(path4,['chunk_',num2str(i),'_0_',num2str(id),'_solid_leftb_3D.fuse']);
        id=id+1;
        copyfile(fullfile(path2,files(j).name),a2);
    end
    
    filename=['chunk_',num2str(i),'_*dashed_middle_3D.fuse'];
    files=dir(fullfile(path2,filename));
    for j=1:length(files)
        a2=fullfile(path4,['chunk_',num2str(i),'_0_',num2str(id),'_dashed_middle_3D.fuse']);
        id=id+1;
        copyfile(fullfile(path2,files(j).name),a2);
    end
    
    filename=['chunk_',num2str(i),'_*solid_rightb_3D.fuse'];
    files=dir(fullfile(path2,filename));
    for j=1:length(files)
        a2=fullfile(path4,['chunk_',num2str(i),'_0_',num2str(id),'_solid_rightb_3D.fuse']);
        id=id+1;
        copyfile(fullfile(path2,files(j).name),a2);
    end
    
    filename=['chunk_',num2str(i),'_*boundary_rightb*.fuse'];
    files=dir(fullfile(path3,filename));
    a2=fullfile(path4,['chunk_',num2str(i),'_0_',num2str(id),'_boundary_rightb_3D.fuse']);
    id=id+1;
    copyfile(fullfile(path3,files(1).name),a2);
    
 
end