path3='F:\HERE\boundary_all4';
need_interpolate=[];
%for i=start:endd
for i=5026:5050
    filename=['chunk_',num2str(i),'*_boundary_leftb_3D.fuse'];
    files=dir(fullfile(path3,filename));
    
    ofiles=files;
    if isempty(files)
        need_interpolate=[need_interpolate;i];
        continue;
    end
    
    fid=fopen(fullfile(path3,files(1).name));
    A=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A=A';
    [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A0=[x y z];   
    
    solid_left_name=['chunk_',num2str(i),'*solid_leftb*'];
    files=dir(fullfile(path2,solid_left_name));
    
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A=A';
    [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A1=[x y z];   
    
    solid_right_name=['chunk_',num2str(i),'*solid_rightb*'];
    files=dir(fullfile(path2,solid_right_name));
    
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A=A';
    [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A2=[x y z];  
    
    p=polyfit(A1(:,1),A1(:,2),1);
    dist=(p(1)*A0(:,1)-A0(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    p2=polyfit(A2(:,1),A2(:,2),1);
    dist2=(p2(1)*A0(:,1)-A0(:,2)+p2(2))/sqrt(p2(1)*p2(1)+1);
    
    if sum(dist<0)<sum(dist>0) && sum(dist2>0)<sum(dist2<0) && abs(mean(dist))>.2 && abs(mean(dist2))>.2
        need_interpolate=[need_interpolate;i];
        movefile(fullfile(path3,ofiles(1).name),fullfile(path3,'old',ofiles(1).name));
    elseif sum(dist>0)<sum(dist<0) && sum(dist2<0)<sum(dist2>0) && abs(mean(dist))>.2 && abs(mean(dist2))>.2
        need_interpolate=[need_interpolate;i];
        movefile(fullfile(path3,ofiles(1).name),fullfile(path3,'old',ofiles(1).name));
    end
end

for i=1:length(need_interpolate)
        n=need_interpolate(i);
        for j=n-1:-1:start
            if ~ismember(j,need_interpolate)
                break;
            end
        end
        for k=n+1:endd
            if ~ismember(k,need_interpolate)
                break;
            end
        end
        
        filename=['chunk_',num2str(j),'*_boundary_leftb_3D.fuse'];
        files=dir(fullfile(path3,filename));

        f1=textread(fullfile(path3,files(1).name));
        filename=['chunk_',num2str(k),'*_boundary_leftb_3D.fuse'];
        files=dir(fullfile(path3,filename));
        f2=textread(fullfile(path3,files(1).name));
        mean_f1=mean(f1);
        mean_f2=mean(f2);
        f3=f1+repmat((n-j)/(k-j)*(mean_f2-mean_f1),size(f1,1),1);
        
        fid3=fopen([path3,'\','chunk_',num2str(n),'_0_0_boundary_leftb_.fuse'],'w');
        fprintf(fid3,'%.15f %.15f %.15f %.0f\n',f3');
        fclose(fid3);     
end


for i=1:length(need_interpolate)
        n=need_interpolate(i);
        for j=n-1:-1:start
            if ~ismember(j,need_interpolate)
                break;
            end
        end
        for k=n+1:endd
            if ~ismember(k,need_interpolate)
                break;
            end
        end
        
        filename=['chunk_',num2str(j),'*_boundary_rightb_3D.fuse'];
        files=dir(fullfile(path3,filename));

        f1=textread(fullfile(path3,files(1).name));
        filename=['chunk_',num2str(k),'*_boundary_rightb_3D.fuse'];
        files=dir(fullfile(path3,filename));
        f2=textread(fullfile(path3,files(1).name));
        mean_f1=mean(f1);
        mean_f2=mean(f2);
        f3=f1+repmat((n-j)/(k-j)*(mean_f2-mean_f1),size(f1,1),1);
        
        fid3=fopen([path3,'\','chunk_',num2str(n),'_0_0_boundary_rightb_.fuse'],'w');
        fprintf(fid3,'%.15f %.15f %.15f %.0f\n',f3');
        fclose(fid3);     
end


