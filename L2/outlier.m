for i=start+1:endd-1
    filename=['chunk_',num2str(i),'*_solid_leftb_3D.fuse'];
    files=dir(fullfile(path2,filename));
 
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A=A';
    [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A=[x y z];   
   
    filename=['chunk_',num2str(i-1),'*_solid_leftb_3D.fuse'];
    files=dir(fullfile(path2,filename));
 
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A2=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A2=A2';
    
    [x,y,z]=geodetic2ned(A2(:,1),A2(:,2),A2(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A2=[x y z];   
   
    filename=['chunk_',num2str(i+1),'*_solid_leftb_3D.fuse'];
    files=dir(fullfile(path2,filename));
 
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A3=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A3=A3';
    [x,y,z]=geodetic2ned(A3(:,1),A3(:,2),A3(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A3=[x y z];   
    
    p=polyfit(A(:,1),A(:,2),1);
    
    dist=abs(p(1)*A2(:,1)-A2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res(i-start,1)=i;
    res(i-start,2)=min(dist);
    
    dist=abs(p(1)*A3(:,1)-A3(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res(i-start,3)=min(dist);
    
    p=polyfit(A2(:,1),A2(:,2),1);
    dist=abs(p(1)*A3(:,1)-A3(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res(i-start,4)=min(dist);
end

need_check=[];
for i=1:size(res,1)
    if res(i,2)>0.1 || res(i,3)>0.1
        need_check=[need_check;res(i,:)];
    end
end

need_interpolate=[];
i=1;
while i<=size(need_check,1)
    for j=i:size(need_check,1)+1
        if j>size(need_check,1) || need_check(j,1)-need_check(i,1)>j-i
            break;
        end
    end
    if j-i>=3
        for j=i+1:j-2
            need_interpolate=[need_interpolate;need_check(j,1)];
        end
    end
    i=j;
end


path2=['F:\HERE\chucai_old'];
res2=zeros(0,3);
start=4881;
for i=start+1:endd-1
    filename=['chunk_',num2str(i),'*_solid_rightb_3D.fuse'];
    files=dir(fullfile(path2,filename));
 
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A=A';
    [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A=[x y z];  
   
    filename=['chunk_',num2str(i-1),'*_solid_rightb_3D.fuse'];
    files=dir(fullfile(path2,filename));
 
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A2=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A2=A2';
    [x,y,z]=geodetic2ned(A2(:,1),A2(:,2),A2(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A2=[x y z];
    
    filename=['chunk_',num2str(i+1),'*_solid_rightb_3D.fuse'];
    
    files=dir(fullfile(path2,filename));
 
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A3=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A3=A3';
     [x,y,z]=geodetic2ned(A3(:,1),A3(:,2),A3(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A3=[x y z];   
    
    
    p=polyfit(A(:,1),A(:,2),1);
    
    dist=abs(p(1)*A2(:,1)-A2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res2(i-start,1)=i;
    res2(i-start,2)=min(dist);
    
    dist=abs(p(1)*A3(:,1)-A3(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res2(i-start,3)=min(dist);
    
    p=polyfit(A2(:,1),A2(:,2),1);
    dist=abs(p(1)*A3(:,1)-A3(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res2(i-start,4)=min(dist);
end


path2=['F:\HERE\chucai_old'];
res3=zeros(0,3);
start=4881;
for i=start+1:endd-1
    filename=['chunk_',num2str(i),'*_dashed_middle*.fuse'];
    files=dir(fullfile(path2,filename));
 
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A=A';
    [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A=[x y z];  
   
    filename=['chunk_',num2str(i-1),'*_dashed_middle*.fuse'];
    files=dir(fullfile(path2,filename));
 
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A2=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A2=A2';
    [x,y,z]=geodetic2ned(A2(:,1),A2(:,2),A2(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A2=[x y z];
    
    filename=['chunk_',num2str(i+1),'*_dashed_middle*.fuse'];
    
    files=dir(fullfile(path2,filename));
 
    boundary_out_fullname=fullfile(path2,files(1).name);
    fid=fopen(boundary_out_fullname);
    A3=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A3=A3';
     [x,y,z]=geodetic2ned(A3(:,1),A3(:,2),A3(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A3=[x y z];   
    
    
    p=polyfit(A(:,1),A(:,2),1);
    
    dist=abs(p(1)*A2(:,1)-A2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res3(i-start,1)=i;
    res3(i-start,2)=min(dist);
    
    dist=abs(p(1)*A3(:,1)-A3(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res3(i-start,3)=min(dist);
    
    p=polyfit(A2(:,1),A2(:,2),1);
    dist=abs(p(1)*A3(:,1)-A3(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res3(i-start,4)=min(dist);
end
need_interpolate=[5658];