line_num=max(res_all(:,3));

fid=fopen('res.txt','w');
for i=1:line_num
    tmp=res_all(res_all(:,3)==i,:);
    if sum(tmp(:,1))~=sum(tmp(:,2))
        for j=1:size(tmp,1)
            fprintf(fid,'%d %d %d %d\n',tmp(j,4),tmp(j,5)-1,tmp(j,1)-1,tmp(j,2)-1);
        end
        fprintf(fid,'\n');
    end
    
end

fclose(fid);