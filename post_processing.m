sorted=lines;
sorted(:,5)=my_res_mat';
sorted=sortrows(sorted,[2,3]);

for i=start:endd
    tmp=sorted(sorted(:,2)==i,:);
    ids=tmp(:,5);
    flagg=0;
    
    solid=ones(1,length(ids));
    for j=1:length(ids)
        if ids(j)==3
            flag=0;
            for k1=j-1:-1:1
                if ids(k1)<3
                    flag=flag+1;
                    break;
                end
            end
            for k2=j+1:length(ids)
                if ids(k2)<3
                    flag=flag+1;
                    break;
                end
            end
            if flag==2
                solid(j)=3;
                solid(k1)=ids(k1);
                solid(k2)=ids(k2);
            end
        end
    end
    sorted(sorted(:,2)==i,5)=solid;
end

sorted(:,5)=sorted(:,5)-1;