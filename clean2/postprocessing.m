%% read test result
sorted=lines;
sorted(:,5)=my_res_mat';
sorted=sortrows(sorted,[2,3]);

%% compute distance to trajectory
path2='F:\HERE\mergedLanes1_all';
for i=start:endd
    pose_file=fullfile(path2,['chunk_',num2str(i),'_pose.fuse']);
    
    fid2=fopen(pose_file);
    A2=fscanf(fid2,'%f %f %f %*f',[3 inf]);
    fclose(fid2);
    A2=A2';
    [x,y,z]=geodetic2ned(A2(:,1),A2(:,2),A2(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    a2=[x y z];
    
    l=sorted(sorted(:,2)==i,:);
    
    for j=1:size(l,1)
        fname=['chunk_',num2str(l(j,2)),'_',num2str(l(j,3)),'.ned'];
        fid=fopen(fullfile(path2,fname));
        A=fscanf(fid,'%f %f %f',[3 inf]);
        fclose(fid);
        A=A';
        p=polyfit(A(:,1),A(:,2),1);
        dist=abs(p(1)*a2(:,1)-a2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
        sorted(sorted(:,2)==l(j,2) & sorted(:,3)==l(j,3),6)=mean(dist);
    end
end

%% use rules for post processing
for i=start:endd
     l=sorted(sorted(:,2)==i,:);
     new_l=zeros(size(l,1),1);
     if sum(l(:,5)==2)==0 
         [~,sortIndex]=sort(l(:,6),'ascend');
         solid_cnt=1;
         for j=1:length(sortIndex)
             if l(sortIndex(j),5)==1 && solid_cnt<=2
                 new_l(sortIndex(j))=1;
                 solid_cnt=solid_cnt+1;
             end
         end
         sorted(sorted(:,2)==i,7)=new_l;
     elseif sum(l(:,5)==2)==1
         pos=find(l(:,5)==2);
         new_l(pos)=2;
         for j=pos-1:-1:1
             if l(j,5)==1
                 break;
             end
         end
         if l(j,5)==1
             new_l(j)=1;
         end
         for j=pos+1:size(l,1)
             if l(j,5)==1
                 break;
             end
         end
         if l(j,5)==1
             new_l(j)=1;
         end
         sorted(sorted(:,2)==i,7)=new_l;
     elseif sum(l(:,5)==2)>1
         [~,sortIndex]=sort(l(:,6),'ascend');
         for j=1:length(sortIndex)
             if l(sortIndex(j),5)==2
                 break;
             end
         end
         p=sortIndex(j);
         new_l(p)=2;
         for k=p-1:-1:1
             new_l(k)=l(k,5);
             if l(k,5)==1
                break;
             end
         end
         for k=p+1:size(l,1)
             new_l(k)=l(k,5);
             if l(k,5)==1
                 break;
             end
         end
         sorted(sorted(:,2)==i,7)=new_l;
     end
end