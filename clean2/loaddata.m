%% load fuse and convert to ned

start=678;
endd=809;

%path='E:\marking\6944320-mergedLanes';
path='F:HERE\SF_lane_marking';
path2='F:HERE\SF_lane_marking_out';
for i=start:endd
    boundary_name=['chunk_',num2str(i),'_0_*.fuse'];
    files=dir(fullfile(path,boundary_name));
    idd=1;
    for j=1:length(files)
        fid2=fopen(fullfile(path,files(j).name));
        A2=fscanf(fid2,'%f %f %f %*f',[3 inf]);
        fclose(fid2);
        A2=A2';
        [x,y,z]=geodetic2ned(A2(:,1),A2(:,2),A2(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
        boundary_outname=['chunk_',num2str(i),'_',num2str(idd),'.ned'];
        idd=idd+1;
        boundary_out_fullname=fullfile(path2,boundary_outname);
        fid=fopen(boundary_out_fullname,'w');
        fprintf(fid,'%f %f %f\n',[x,y,z]');
        fclose(fid);
      %  dlmwrite(boundary_out_fullname,[x y z],' ');
    end
    
    if isempty(files)
        continue;
    end
end

