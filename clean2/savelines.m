%% compute distance
res=zeros(9000,10);
lane_id=1;


%% group lines
for i=start:endd-1
    boundary_name=['chunk_',num2str(i),'_*.ned'];
    files=dir(fullfile(path2,boundary_name));
    p1=cell(1,length(files));
    for j=1:length(files)
        fid=fopen(fullfile(path2,files(j).name));
        A=fscanf(fid,'%f %f %f',[3 inf]);
        fclose(fid);
        A=A';
        p1{1,j}=A;
    end
    
    boundary_name2=['chunk_',num2str(i+1),'_*.ned'];
    files2=dir(fullfile(path2,boundary_name2));
    p2=cell(1,length(files2));
    for k=1:length(files2)
        fid=fopen(fullfile(path2,files2(k).name));
        A=fscanf(fid,'%f %f %f',[3 inf]);
        fclose(fid);
        A=A';
        p2{1,k}=A;
    end
    
    for j=1:length(files)
        if res(i,j)==0
            res(i,j)=lane_id;
            lane_id=lane_id+1;
        end
    end
    
    for j=1:size(p1,2)
        for k=1:size(p2,2)
            a1=p1{1,j};
            a2=p2{1,k};
            p=polyfit(a1(:,1),a1(:,2),1);
            dist=abs(p(1)*a2(:,1)-a2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
            if min(dist)<0.3
                res(i+1,k)=res(i,j);
            end
        end
    end
end

%% save files into txt including labels (if for testing no labels then set label to 0
label=zeros(8000,10);

%% for training only
for i=start:endd
    fid=fopen([path2,'\gt\',num2str(i),'.gt']);
    tline=fgetl(fid);
    j=1;
    while ischar(tline)
        ts=strsplit(tline,', ');
        label(i,j)=str2num(ts{2});
        j=j+1;
        tline = fgetl(fid);
    end
    fclose(fid);
end
%% end for training only

line_num=max(max(res));
fid=fopen(fullfile(path2, save_file),'w');
for i=1:line_num
    [x, y]=find(res==i);
    xy=sortrows([x,y]);
    for j=1:size(xy,1)
        fprintf(fid,'%d %d %d %d\n',i, xy(j,1),xy(j,2),label(xy(j,1),xy(j,2)));
    end
end
fclose(fid);