sorted2=sortrows(sorted,1);
cnt=0;
for i=1:line_num
    if i==1996
        i
    end
    tmp=sorted2(sorted2(:,1)==i,:);
    ids=tmp(:,5);
   
    if sum(ids>0)<=2 && length(ids)>=4
        sorted2(sorted2(:,1)==i,6)=0;
        continue;
    end
     if sum(ids>0)<=1 && length(ids)>=3
        sorted2(sorted2(:,1)==i,6)=0;
        continue;
    end
    for j=1:length(ids)
        if ids(j)==0
            flag1=0;
            flag2=0;
            for k1=j-1:-1:max(1,j-3)
                if ids(k1)>0
                    flag1=ids(k1);
                    break;
                end
            end
            for k2=j+1:min(j+3,length(ids))
                if ids(k2)>0
                    flag2=ids(k2);
                    break;
                end
            end
            if flag1==flag2
                ids(j)=flag1;
            end
            if j<=2 && flag2>0 
                ids(j)=flag2;
            end
            if j>=length(ids)-1 && flag1>0
                ids(j)=flag1;
            end
        end
    end
    if length(ids)<2
        ids=0;
    end
    sorted2(sorted2(:,1)==i,6)=ids;
end