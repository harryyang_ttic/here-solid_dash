function [] = FillMissingLaneMark( R,start,endd,LR,my_path, mean_A )
%FILLMISSINGLANEMARK Summary of this function goes here
%   Detailed explanation goes here
need_interpolate=[];
for i=start:endd
    ids=R(R(:,1)==i,:);
    ids_1=find(ids==1);
    if isempty(ids_1)
        need_interpolate=[need_interpolate;i];
    else
        ids_2=find(ids==2);
        if ~isempty(ids_2) && ids_2(1)<ids_1(1)
            need_interpolate=[need_interpolate;i];
        end
        if isempty(ids_2)
            files=dir([my_path,'\chunk_',num2str(i),'*_solid_',LR,'*']);
            if length(files)==0
                need_interpolate=[need_interpolate;i];
            end
        end
    end
end

for i=1:length(need_interpolate)
    n=need_interpolate(i);
    for j1=n-1:-1:start
        ids=R(R(:,1)==j1,:);
        ids_11=find(ids==1);
        if ~isempty(ids_11)
            ids_2=find(ids==2);
            if ~isempty(ids_2) && ids_2(1)>ids_11(1)
                break;
            end
        end
    end
    for j2=n+1:endd
        ids=R(R(:,1)==j2,:);
        ids_12=find(ids==1);
        if ~isempty(ids_12)
            ids_2=find(ids==2);
            if ~isempty(ids_2) && ids_2(1)>ids_12(1)
                break;
            end
        end
    end
    f1=textread([my_path,'\chunk_',num2str(j1),'_',num2str(ids_11(1)-1),'_solid_',LR,'_3D.fuse']);
    f2=textread([my_path,'\chunk_',num2str(j2),'_',num2str(ids_12(1)-1),'_solid_',LR,'_3D.fuse']);
    
    [x,y,z]=geodetic2ned(f1(:,1),f1(:,2),f1(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A1=[x y z];
    [x,y,z]=geodetic2ned(f2(:,1),f2(:,2),f2(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A2=[x y z];
              
    %{
    first_pt=A1(end,:);
    last_pt=A2(1,:);
    %}
    D=pdist2(A1,A2);
    [id1,id2]=find(D==min(min(D)));
    first_pt=A1(id1(1),:);
    last_pt=A2(id2(1),:);
    
    first_pt1=first_pt+(last_pt-first_pt)*(n-j1-1)/(j2-j1-1);
    last_pt=first_pt1+1/(j2-j1-1)*(last_pt-first_pt);
    new_A(1,1:3)=first_pt1;
    for k=1:5000
        cur_pt=first_pt1+k/5000*(last_pt-first_pt1);
        new_A(k+1,1:3)=cur_pt;
    end
    
    [lat,lon,h]=ned2geodetic(new_A(:,1),new_A(:,2),new_A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
     
    fid3=fopen([my_path,'\chunk_',num2str(n),'_1_solid_',LR,'_3D_interpolated.fuse'],'w');
    fprintf(fid3,'%.15f %.15f %.15f 255\n',[lat,lon,h]');
    fclose(fid3);

end

end