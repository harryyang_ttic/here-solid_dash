function [ output_args ] = FillOutlierLaneMark( R,start,endd,LR,path3,mean_A )
%FILLOUTLIERLANEMARK Summary of this function goes here
%   Detailed explanation goes here
%LR='leftb';

res=[];
for i=start:endd
    filename=['chunk_',num2str(i),'*_solid_',LR,'_3D*.fuse'];
    files=dir(fullfile(path3,filename));
    boundary_out_fullname=fullfile(path3,files(1).name);
    fid=fopen(boundary_out_fullname);
    A=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A=A';
    [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A=[x y z];   
   
    filename=['chunk_',num2str(i),'*_pose.fuse'];
    files=dir(fullfile('F:\HERE\pose',filename));
 
    boundary_out_fullname=fullfile('F:\HERE\pose',files(1).name);
    fid=fopen(boundary_out_fullname);
    A2=fscanf(fid,'%f %f %f %*f',[3 inf]);
    fclose(fid);
    A2=A2';
    [x,y,z]=geodetic2ned(A2(:,1),A2(:,2),A2(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    A2=[x y z];   
      
    p=polyfit(A2(:,1),A2(:,2),1);
    
    dist=(p(1)*A(:,1)-A(:,2)+p(2))/sqrt(p(1)*p(1)+1);
    res(i-start+1,1)=i;
    res(i-start+1,2)=mean(dist);
 
end

%% get pieces
id=1;
res(1,3)=id;
for i=start+1:endd
    if abs(res(i-start+1,2)-res(i-start,2))>1
        id=id+1;
    end
    res(i-start+1,3)=id;
end

end

