function [ output_args ] = ReOrder( my_path,pose_path, start,endd, mean_A )
%REORDER Summary of this function goes here
%   Detailed explanation goes here
for i=start:endd
    pose_file=fullfile(pose_path,['chunk_',num2str(i),'_pose.fuse']);
    
    fid2=fopen(pose_file);
    A2=fscanf(fid2,'%f %f %f %*f',[3 inf]);
    fclose(fid2);
    A2=A2';
    [x,y,z]=geodetic2ned(A2(:,1),A2(:,2),A2(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    a2=[x y z];
    
    fname=['chunk_',num2str(i),'*.fuse'];
    files=dir(fullfile(my_path,fname));
    p=polyfit(a2(:,1),a2(:,2),1);
    dist_all=[];
    for j=1:size(files,1)
        fid=fopen(fullfile(my_path,files(j).name));
        A=fscanf(fid,'%f %f %f %*f',[3 inf]);
        fclose(fid);
        A=A';
        [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
        A=[x y z];
        dist=(p(1)*A(:,1)-A(:,2)+p(2))/sqrt(p(1)*p(1)+1);
        dist_all(j)=mean(dist);
    end
    
    [~,ids]=sort(dist_all,'ascend');
    boundary_id=1;
    solid_id=1;
    for j=1:length(ids)
        k=strfind(files(ids(j)).name,'boundary');
        if length(k)>0 && dist_all(ids(j))<0
            new_name=['chunk_',num2str(i),'_',num2str(j-1),'_left_boundary_3D.fuse'];
            boundary_id=boundary_id+1;
        elseif length(k)>0 && dist_all(ids(j)>=0)
            new_name=['chunk_',num2str(i),'_',num2str(j-1),'_right_boundary_3D.fuse'];
        end
        k=strfind(files(ids(j)).name,'solid');
        if length(k)>0 &&  dist_all(ids(j))<0
           new_name=['chunk_',num2str(i),'_',num2str(j-1),'_solid_leftb_3D.fuse'];
           solid_id=solid_id+1;
        elseif length(k)>0 && dist_all(ids(j))>=0
            new_name=['chunk_',num2str(i),'_',num2str(j-1),'_solid_rightb_3D.fuse'];
        end
        k=strfind(files(ids(j)).name,'dashed');
        if length(k)>0
            new_name=['chunk_',num2str(i),'_',num2str(j-1),'_dashed_middle_3D.fuse'];
        end
        movefile(fullfile(my_path,files(ids(j)).name),fullfile(my_path,new_name));
    end
    
    
end


end

