S=[S2;S3];

acc_folder='F:\HERE\6944320_acc_r';

acc_list=textread(fullfile(acc_folder,'6944320_accessor_chunk_list.csv'));

D_all=zeros(0,10);
for i=1:length(acc_list)-1
    fid=fopen(fullfile(acc_folder,[num2str(acc_list(i)),'.acc']));
    A=fscanf(fid,'%f,%f,%f\n');
    fclose(fid);
    A=reshape(A,3,[]);
    A=A';
    
    start=acc_list(i);
    endd=acc_list(i+1);
    
    [x,y,z]=geodetic2ned(A(:,1),A(:,2),A(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    M=[x,y,z];
   
    lines=S(S(:,2)==start,:);
    p1=cell(1,size(lines,1));
   
    for k=1:size(lines,1)
        boundary_outname=['chunk_',num2str(lines(k,2)),'_',num2str(lines(k,3)),'.ned'];
        boundary_out_fullname=fullfile(path2,boundary_outname);
        fid=fopen(boundary_out_fullname);
        A=fscanf(fid,'%f %f %f',[3 inf]);
        fclose(fid);
        A=A';
        p1{1,k}=A;
    end
    
   D=zeros(size(lines,1),size(M,1)-2);
   for j=1:size(p1,2)
        a1=p1{1,j};
        p=polyfit(a1(:,1),a1(:,2),1);
        for k=2:size(M,1)-1
            dist=abs(p(1)*M(k,1)-M(k,2)+p(2))/sqrt(p(1)*p(1)+1);
            D(j,k-1)=dist;
        end
   end
   D_all(i,1:size(D,2))=min(D);
end

E=zeros(0,10);
for i=1:length(acc_list)-1
    fid=fopen(fullfile(acc_folder,[num2str(acc_list(i)),'.acc']));
    A=fscanf(fid,'%f,%f,%f\n');
    fclose(fid);
    A=reshape(A,3,[]);
    A=A';
  
    
    start=acc_list(i);
    E(i,1)=start;
    E(i,2)=size(A,1)-2;
    lines=S(S(:,2)==start,:);
    t=lines(:,6);
    t(t==0)=[];
    E(i,3:3+length(t)-1)=t;
    n=sum(lines(:,6)>0);
    
    p1=cell(1,size(lines,1));
   
    for k=1:size(lines,1)
        boundary_outname=['chunk_',num2str(lines(k,2)),'_',num2str(lines(k,3)),'.ned'];
        boundary_out_fullname=fullfile(path2,boundary_outname);
        fid=fopen(boundary_out_fullname);
        A=fscanf(fid,'%f %f %f',[3 inf]);
        fclose(fid);
        A=A';
        p1{1,k}=A;
    end
    
   D=zeros(size(lines,1),size(M,1)-2);
   for j=1:size(p1,2)
        a1=p1{1,j};
        p=polyfit(a1(:,1),a1(:,2),1);
        for k=2:size(M,1)-1
            dist=abs(p(1)*M(k,1)-M(k,2)+p(2))/sqrt(p(1)*p(1)+1);
            D(j,k-1)=dist;
        end
   end
   D_all(i,1:size(D,2))=min(D);
end