%% compute distance
res=zeros(9000,10);
lane_id=1;

for i=start:endd-1
    boundary_name=['chunk_',num2str(i),'_*.ned'];
    files=dir(fullfile(path2,boundary_name));
    p1=cell(1,length(files));
    for j=1:length(files)
        fid=fopen(fullfile(path2,files(j).name));
        A=fscanf(fid,'%f %f %f',[3 inf]);
        fclose(fid);
        A=A';
        p1{1,j}=A;
    end
    
    boundary_name2=['chunk_',num2str(i+1),'_*.ned'];
    files2=dir(fullfile(path2,boundary_name2));
    p2=cell(1,length(files2));
    for k=1:length(files2)
        fid=fopen(fullfile(path2,files2(k).name));
        A=fscanf(fid,'%f %f %f',[3 inf]);
        fclose(fid);
        A=A';
        p2{1,k}=A;
    end
    
    for j=1:length(files)
        if res(i,j)==0
            res(i,j)=lane_id;
            lane_id=lane_id+1;
        end
    end
    
    for j=1:size(p1,2)
        for k=1:size(p2,2)
            a1=p1{1,j};
            a2=p2{1,k};
            p=polyfit(a1(:,1),a1(:,2),1);
            dist=abs(p(1)*a2(:,1)-a2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
            if min(dist)<0.3
                res(i+1,k)=res(i,j);
            end
        end
    end
end

%% save files
label=zeros(8000,10);
for i=start:endd
    fid=fopen([path2,'\gt\',num2str(i),'.gt']);
    tline=fgetl(fid);
    j=1;
    while ischar(tline)
        ts=strsplit(tline,', ');
        label(i,j)=str2num(ts{2});
        j=j+1;
        tline = fgetl(fid);
    end
    fclose(fid);
end

line_num=max(max(res));
fid=fopen(fullfile(path2, save_file),'w');
for i=1:line_num
    [x, y]=find(res==i);
    xy=sortrows([x,y]);
    for j=1:size(xy,1)
        fprintf(fid,'%d %d %d %d\n',i, xy(j,1),xy(j,2),label(xy(j,1),xy(j,2)));
    end
end
fclose(fid);

lines=textread([path2 save_file]);
line_num=max(lines(:,1));
id=1;
res=zeros(0,7);
for i=1:line_num
    files=lines(lines(:,1)==i,:);
    if sum(files(:,4)==0)>0
        continue;
    end
    for j=1:size(files,1)
        in_name=([path2,'\chunk_',num2str(files(j,2)),'_',num2str(files(j,3)),'.ned']);
        in_pts=textread(in_name);
        p=polyfit(in_pts(:,1),in_pts(:,2),1);
        if p(1)==inf || p(1)==-inf
            vec=[0,1];
            vec2=[in_pts(:,1)-in_pts(1,1), in_pts(:,2)];
            dist3=vec2(:,1).*vec(1)+vec2(:,2).*vec(2);
        else
            vec=[1/sqrt(p(1)*p(1)+1),p(1)/sqrt(p(1)*p(1)+1)];
            vec2=[in_pts(:,1), in_pts(:,2)-p(2)];
            dist3=vec2(:,1).*vec(1)+vec2(:,2).*vec(2);
        end
        binranges=min(dist3):0.2:max(dist3);
        hist=histc(dist3,binranges);
        ratio=sum(hist==0)/(sum(hist==0)+sum(hist~=0));
        
        if j>1
            in_name=([path2,'\chunk_',num2str(files(j-1,2)),'_',num2str(files(j-1,3)),'.ned']);
            in_pts=[in_pts; textread(in_name)];
        end
        if j<size(files,1)
            in_name=([path2,'\chunk_',num2str(files(j+1,2)),'_',num2str(files(j+1,3)),'.ned']);
            in_pts=[in_pts; textread(in_name)];
        end
        
        p=polyfit(in_pts(:,1),in_pts(:,2),1);
        if p(1)==inf || p(1)==-inf
            vec=[0,1];
            vec2=[in_pts(:,1)-in_pts(1,1), in_pts(:,2)];
            dist3=vec2(:,1).*vec(1)+vec2(:,2).*vec(2);
        else
            vec=[1/sqrt(p(1)*p(1)+1),p(1)/sqrt(p(1)*p(1)+1)];
            vec2=[in_pts(:,1), in_pts(:,2)-p(2)];
            dist3=vec2(:,1).*vec(1)+vec2(:,2).*vec(2);
        end
        binranges=min(dist3):0.2:max(dist3);
        hist=histc(dist3,binranges);
        ratio2=sum(hist==0)/(sum(hist==0)+sum(hist~=0));
        
        hist_len=[];
        hist_start=1;
        for k=2:length(hist)
            if hist(k)==0 && hist(k-1)>0
                hist_endd=k-1;
                hist_len=[hist_len,hist_endd-hist_start+1];
                hist_start=k;
            elseif hist(k)>0 && hist(k-1)==0
                hist_endd=k-1;
                hist_len=[hist_len,hist_endd-hist_start+1];
                hist_start=k;
            end
        end
        if(length(hist_len)<3)
            mean_len=mean(hist_len);
            var_len=var(hist_len);
        else
            if hist_len(1)<3
                hist_len(1)=[];
            end
            if hist_len(length(hist_len))<3
                hist_len(length(hist_len))=[];
            end
            mean_len=mean(hist_len);
            var_len=var(hist_len);
        end
        
        res(id,1)=i;
        res(id,2)=j;
        res(id,3)=ratio;
        res(id,4)=ratio2;
        res(id,5)=mean_len;
        res(id,6)=var_len;
        res(id,7)=files(j,4);
        id=id+1;
    end
end

%% train
num=max(res(:,1));
res2=res;
obsData={};
hiddenData={};
id=1;
for i=1:num
    if sum(res2(:,1)==i)==0
        continue;
    end
    obsData{id}=res2(res2(:,1)==i,[3:5])';
    hiddenData{id}=res2(res2(:,1)==i,7)';
    id=id+1;
end
[initState,transmat,mu,Sigma]=gausshmm_train_observed(obsData,hiddenData,2);
