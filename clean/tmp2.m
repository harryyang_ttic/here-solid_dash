path='F:HERE\SF_lane_marking';
path2='F:HERE\SF_lane_marking_L1';
for i=start:endd
    boundary_name=['chunk_',num2str(i),'_0_*.fuse'];
    files=dir(fullfile(path,boundary_name));
    id=1;
    for j=1:length(files)
        label=sorted(sorted(:,2)==i & sorted(:,3)==j,7);
        if label==1 && id==1
            name_out=fullfile(path2,['chunk_',num2str(i),'_',num2str(j),'_solid_leftb_3D.fuse']);
            copyfile(fullfile(path,files(j).name),name_out);
            id=id+1;
        elseif label==1 && id>1
            name_out=fullfile(path2,['chunk_',num2str(i),'_',num2str(j),'_solid_rightb_3D.fuse']);
            copyfile(fullfile(path,files(j).name),name_out);
        elseif label==2 
            name_out=fullfile(path2,['chunk_',num2str(i),'_',num2str(j),'_dashed_middle_3D.fuse']);
            copyfile(fullfile(path,files(j).name),name_out);
        end
    end
end
