start=678;
endd=809;

%path='E:\marking\6944320-mergedLanes';
path='F:HERE\SF_lane_marking';
path2='F:HERE\SF_lane_marking_out';
for i=start:endd
    boundary_name=['chunk_',num2str(i),'_0_*.fuse'];
    files=dir(fullfile(path,boundary_name));
    idd=1;
    for j=1:length(files)
        fid2=fopen(fullfile(path,files(j).name));
        A2=fscanf(fid2,'%f %f %f %*f',[3 inf]);
        fclose(fid2);
        A2=A2';
        [x,y,z]=geodetic2ned(A2(:,1),A2(:,2),A2(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
        boundary_outname=['chunk_',num2str(i),'_',num2str(idd),'.ned'];
        idd=idd+1;
        boundary_out_fullname=fullfile(path2,boundary_outname);
        fid=fopen(boundary_out_fullname,'w');
        fprintf(fid,'%f %f %f\n',[x,y,z]');
        fclose(fid);
      %  dlmwrite(boundary_out_fullname,[x y z],' ');
    end
    
    if isempty(files)
        continue;
    end
end


%% compute distance
res=zeros(endd-start,10);

for i=start:endd-1
    boundary_name=['chunk_',num2str(i),'_*.ned'];
    files=dir(fullfile(path2,boundary_name));
    p1=cell(1,length(files));
    for j=1:length(files)
        fid=fopen(fullfile(path2,files(j).name));
        A=fscanf(fid,'%f %f %f',[3 inf]);
        fclose(fid);
        A=A';
        p1{1,j}=A;
    end
    
    boundary_name2=['chunk_',num2str(i+1),'_*.ned'];
    files2=dir(fullfile(path2,boundary_name2));
    p2=cell(1,length(files2));
    for k=1:length(files2)
        fid=fopen(fullfile(path2,files2(k).name));
        A=fscanf(fid,'%f %f %f',[3 inf]);
        fclose(fid);
        A=A';
        p2{1,k}=A;
    end
    
    for j=1:size(p1,2)
        for k=1:size(p2,2)
            a1=p1{1,j};
            a2=p2{1,k};
            p=polyfit(a1(:,1),a1(:,2),1);
            dist=abs(p(1)*a2(:,1)-a2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
            if min(dist)<0.5
                res(i,j)=k;
            end
        end
    end
end

%% train

res=zeros(1300,10);
lane_id=1;

for i=start:endd-1
    boundary_name=['chunk_',num2str(i),'_*.ned'];
    files=dir(fullfile(path2,boundary_name));
    p1=cell(1,length(files));
    for j=1:length(files)
        fid=fopen(fullfile(path2,files(j).name));
        A=fscanf(fid,'%f %f %f',[3 inf]);
        fclose(fid);
        A=A';
        p1{1,j}=A;
    end
    
    boundary_name2=['chunk_',num2str(i+1),'_*.ned'];
    files2=dir(fullfile(path2,boundary_name2));
    p2=cell(1,length(files2));
    for k=1:length(files2)
        fid=fopen(fullfile(path2,files2(k).name));
        A=fscanf(fid,'%f %f %f',[3 inf]);
        fclose(fid);
        A=A';
        p2{1,k}=A;
    end
    
    for j=1:length(files)
        if res(i,j)==0
            res(i,j)=lane_id;
            lane_id=lane_id+1;
        end
    end
    
    for j=1:size(p1,2)
        for k=1:size(p2,2)
            a1=p1{1,j};
            a2=p2{1,k};
            p=polyfit(a1(:,1),a1(:,2),1);
            dist=abs(p(1)*a2(:,1)-a2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
            if min(dist)<0.5
                res(i+1,k)=res(i,j);
            end
        end
    end
end

%% save lines
line_num=max(max(res));
fid=fopen([path2 '\lines.txt'],'w');
for i=1:line_num
    [x, y]=find(res==i);
    xy=sortrows([x,y]);
    for j=1:size(xy,1)
        fprintf(fid,'%d %d %d\n',i, xy(j,1),xy(j,2));
    end
end

%% save points
lines=textread([path2 save_file]);
line_num=max(lines(:,1));
for i=1:line_num
    files=lines(lines(:,1)==i,:);
    out_name=([path2,'\line\',num2str(i),'.txt']);
    fid=fopen(out_name,'w');
    for j=1:size(files,1)
        in_name=([path2,'\chunk_',num2str(files(j,2)),'_',num2str(files(j,3)),'.ned']);
        in_pts=textread(in_name);
        in_pts(:,4)=j;
        fprintf(fid,'%g %g %g %g\n',in_pts');
    end
    fclose(fid);
end

%% train
lines=textread([path2 '\lines.txt']);
line_num=max(lines(:,1));
id=1;
res=zeros(0,3);
for i=1:line_num
    files=lines(lines(:,1)==i,:);
    for j=1:size(files,1)
        in_name=([path2,'\chunk_',num2str(files(j,2)),'_',num2str(files(j,3)),'.ned']);
        in_pts=textread(in_name);
        p=polyfit(in_pts(:,1),in_pts(:,2),1);
        dist=abs(p(1)*in_pts(:,1)-in_pts(:,2)+p(2))/sqrt(p(1)*p(1)+1);
        dist2=sqrt(in_pts(:,1).*in_pts(:,1)+(in_pts(:,2)-p(2)).*(in_pts(:,2)-p(2)));
        dist3=dist2.*dist2-dist.*dist;
        dist3=sqrt(dist3);
        binranges=min(dist3):0.2:max(dist3);
        hist=histc(dist3,binranges);
        ratio=sum(hist==0)/(sum(hist==0)+sum(hist~=0));
        res(id,1)=i;
        res(id,2)=j;
        res(id,3)=ratio;
        id=id+1;
    end
end

