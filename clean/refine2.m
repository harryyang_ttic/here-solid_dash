lines1=textread('lines_1_8000.txt');
my_res_mat1=textread('my_res_mat_1_8000.txt');
lines1(:,5)=my_res_mat1';
lines2=textread('lines_8001.txt');
my_res_mat2=textread('my_res_mat_8001.txt');
lines2(:,5)=my_res_mat2';

lines2(:,1)=lines2(:,1)+3214;
lines=[lines1;lines2];

sorted=lines;
sorted=sortrows(sorted,[2,3]);

path2='F:\HERE\mergedLanes1_all';
for i=start:endd
    pose_file=fullfile(path2,['chunk_',num2str(i),'_pose.fuse']);
    
    fid2=fopen(pose_file);
    A2=fscanf(fid2,'%f %f %f %*f',[3 inf]);
    fclose(fid2);
    A2=A2';
    [x,y,z]=geodetic2ned(A2(:,1),A2(:,2),A2(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
    a2=[x y z];
    
    l=sorted(sorted(:,2)==i,:);
    
    for j=1:size(l,1)
        fname=['chunk_',num2str(l(j,2)),'_',num2str(l(j,3)),'.ned'];
        fid=fopen(fullfile(path2,fname));
        A=fscanf(fid,'%f %f %f',[3 inf]);
        fclose(fid);
        A=A';
        p=polyfit(A(:,1),A(:,2),1);
        dist=abs(p(1)*a2(:,1)-a2(:,2)+p(2))/sqrt(p(1)*p(1)+1);
        sorted(sorted(:,2)==l(j,2) & sorted(:,3)==l(j,3),6)=mean(dist);
    end
end

for i=start:endd
     l=sorted(sorted(:,2)==i,:);
     new_l=zeros(size(l,1),1);
     if sum(l(:,5)==2)==0 
         [~,sortIndex]=sort(l(:,6),'ascend');
         solid_cnt=1;
         for j=1:length(sortIndex)
             if l(sortIndex(j),5)==1 && solid_cnt<=2
                 new_l(sortIndex(j))=1;
                 solid_cnt=solid_cnt+1;
             end
         end
         sorted(sorted(:,2)==i,7)=new_l;
     elseif sum(l(:,5)==2)==1
         pos=find(l(:,5)==2);
         new_l(pos)=2;
         for j=pos-1:-1:1
             if l(j,5)==1
                 break;
             end
         end
         if l(j,5)==1
             new_l(j)=1;
         end
         for j=pos+1:size(l,1)
             if l(j,5)==1
                 break;
             end
         end
         if l(j,5)==1
             new_l(j)=1;
         end
         sorted(sorted(:,2)==i,7)=new_l;
     elseif sum(l(:,5)==2)>1
         [~,sortIndex]=sort(l(:,6),'ascend');
         for j=1:length(sortIndex)
             if l(sortIndex(j),5)==2
                 break;
             end
         end
         p=sortIndex(j);
         new_l(p)=2;
         for k=p-1:-1:1
             new_l(k)=l(k,5);
             if l(k,5)==1
                break;
             end
         end
         for k=p+1:size(l,1)
             new_l(k)=l(k,5);
             if l(k,5)==1
                 break;
             end
         end
         sorted(sorted(:,2)==i,7)=new_l;
     end
end

for i=1:size(sorted,1)
    sorted(i,8)=label(sorted(i,2),sorted(i,3));
end

acc_folder='F:\HERE\6944320_acc';
acc_list=textread(fullfile(acc_folder,'6944320_accessor_chunk_list.csv'));

R=zeros(size(sorted,1),10);
r=1;
for i=1:length(acc_list)-1
    start=acc_list(i);
    endd=acc_list(i+1)-1;
    for j=start:endd
        l=sorted(sorted(:,2)==j,:);
        R(r,1)=i;
        R(r,2)=j;
        R(r,3:size(l,1)+2)=l(:,7);
        r=r+1;
    end
end