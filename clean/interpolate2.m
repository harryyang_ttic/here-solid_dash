max_acc=max(R(:,1));
sum_row=zeros(0,15);
for i=1:max_acc
    t=R(R(:,1)==i,:);
    t2=[sum(t(:,3:end)==1,2),sum(t(:,3:end)==2,2)];
    [C,~,ic]=unique(t2,'rows');
    sum_row(i,1)=size(C,1);
    for j=1:size(C,1)
        sum_row(i,j+1)=sum(ic==j);
    end
end