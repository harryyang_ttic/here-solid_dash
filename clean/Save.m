path='E:\marking\6944320-mergedLanes';
for i=start:endd
    boundary_name=['chunk_',num2str(i),'_0_*.fuse'];
    files=dir(fullfile(path,boundary_name));
    idd=1;
    fid3=fopen(fullfile(path2,['res_',num2str(i),'.txt']),'w');
    for j=1:length(files) 
        %{
        fid2=fopen(fullfile(path,files(j).name));
       
        A2=fscanf(fid2,'%f %f %f %*f',[3 inf]);
        fclose(fid2);
        A2=A2';
        if size(A2,1)==0
            continue;
        end
        %}
        pos1=find(lines(:,2)==i & lines(:,3)==idd);
        pre1=my_res_mat(pos1(1));
        pos2=find(sorted2(:,2)==i & sorted2(:,3)==idd);
        pre2=sorted2(pos2(1),6);
        fprintf(fid3,[files(j).name,' ',num2str(pre1),' ',num2str(pre2)]);
        fprintf(fid3,'\n');
        idd=idd+1;
       
    end
    fclose(fid3);
    if isempty(files)
        continue;
    end
end


start=1;
endd=8500;

path='E:\marking\6944320-mergedLanes';
path2='F:HERE\mergedLanes1_all4';

%sorted2=sortrows(sorted2,[2,3]);

for i=start:endd
    boundary_name=['chunk_',num2str(i),'_0_*.fuse'];
    files=dir(fullfile(path,boundary_name));
  %  ids=sorted2(sorted2(:,2)==i,6);
    tmp=0;
    row=find(R(:,2)==i);
    if isempty(row)
        continue;
    end
    for j=1:length(files)
        id=R(row,j+2);
        if id==0
            continue;
            %{
            name=['chunk_',num2str(i),'_',num2str(j),'_unknown_3D.fuse'];
            copyfile(fullfile(path,files(j).name),fullfile(path2,name));
            %}
        elseif id==1 && tmp==0
            name=['chunk_',num2str(i),'_',num2str(j),'_solid_leftb_3D.fuse'];
            tmp=1;
            if exist(fullfile(path2,name))
                continue;
            end
            copyfile(fullfile(path,files(j).name),fullfile(path2,name));
        elseif id==1 && tmp>0
            name=['chunk_',num2str(i),'_',num2str(j),'_solid_rightb_3D.fuse'];
            if exist(fullfile(path2,name))
                continue;
            end
            copyfile(fullfile(path,files(j).name),fullfile(path2,name));
        elseif id==2
            name=['chunk_',num2str(i),'_',num2str(j),'_dashed_middle_3D.fuse'];
            tmp=1;
            if exist(fullfile(path2,name))
                continue;
            end
            copyfile(fullfile(path,files(j).name),fullfile(path2,name));
            
        end
            
    end
end

all_solid=[];
for i=1:size(R,1)
    if sum(R(i,3:end)==2)==0 && sum(R(i,3:end)==1)>0
        all_solid=[all_solid;i];
    end
end


path='F:HERE\SF_lane_marking';
path2='F:HERE\SF_lane_marking_L1';
for i=start:endd
    boundary_name=['chunk_',num2str(i),'_0_*.fuse'];
    files=dir(fullfile(path,boundary_name));
    idd=1;
    id=1;
    for j=1:length(files)
        label=sroted(sorted(:,2)==i & sorted(:,3)==j);
        if label==1 && id==1
            name_out=fullfile(path2,['chunk_',num2str(i),'_',num2str(j),'_solid_leftb_3D.fuse']);
            copyfile(fullfile(path,files(j).name),name_out);
            id=id+1;
        elseif label==1 && id>1
            name_out=fullfile(path2,['chunk_',num2str(i),'_',num2str(j),'_solid_righttb_3D.fuse']);
            copyfile(fullfile(path,files(j).name),name_out);
            id=id+1;
        elseif label==2 
            name_out=fullfile(path2,['chunk_',num2str(i),'_',num2str(j),'_dashed_middle_3D.fuse']);
            copyfile(fullfile(path,files(j).name),name_out);
            id=id+1;
        end
        fid2=fopen(fullfile(path,files(j).name));
        A2=fscanf(fid2,'%f %f %f %*f',[3 inf]);
        fclose(fid2);
        A2=A2';
        [x,y,z]=geodetic2ned(A2(:,1),A2(:,2),A2(:,3),mean_A(1),mean_A(2),mean_A(3),wgs84Ellipsoid);
        boundary_outname=['chunk_',num2str(i),'_',num2str(idd),'.ned'];
        idd=idd+1;
        boundary_out_fullname=fullfile(path2,boundary_outname);
        fid=fopen(boundary_out_fullname,'w');
        fprintf(fid,'%f %f %f\n',[x,y,z]');
        fclose(fid);
      %  dlmwrite(boundary_out_fullname,[x y z],' ');
    end
    
    if isempty(files)
        continue;
    end
end
