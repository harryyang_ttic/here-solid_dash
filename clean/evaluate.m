sorted=lines;
sorted(:,5)=my_res_mat';
sorted=sortrows(sorted,[2,3]);

line_num=max(sorted(:,1));
for i=start:endd
    tmp=sorted(sorted(:,2)==i,:);
    ids=tmp(:,5);
    flagg=0;
    
    solid=zeros(1,length(ids));
    for j=1:length(ids)
        if ids(j)==2
            flag=0;
            for k1=j-1:-1:1
                if ids(k1)<2
                    flag=flag+1;
                    break;
                end
            end
            for k2=j+1:length(ids)
                if ids(k2)<2
                    flag=flag+1;
                    break;
                end
            end
            if flag==2
                solid(j)=2;
                solid(k1)=ids(k1);
                solid(k2)=ids(k2);
            end
        end
    end
    sorted(sorted(:,2)==i,5)=solid;
end

sorted2=sortrows(sorted,1);
cnt=0;
for i=1:line_num
    tmp=sorted2(sorted2(:,1)==i,:);
    ids=tmp(:,5);
   
    if sum(ids>0)<=2 && length(ids)>=4
        sorted2(sorted2(:,1)==i,6)=0;
        continue;
    end
     if sum(ids>0)<=1 && length(ids)>=3
        sorted2(sorted2(:,1)==i,6)=0;
        continue;
    end
    for j=1:length(ids)
        if ids(j)==0
            flag1=0;
            flag2=0;
            for k1=j-1:-1:max(1,j-3)
                if ids(k1)>0
                    flag1=ids(k1);
                    break;
                end
            end
            for k2=j+1:min(j+3,length(ids))
                if ids(k2)>0
                    flag2=ids(k2);
                    break;
                end
            end
            if flag1==flag2
                ids(j)=flag1;
            end
            if j<=2 && flag2>0 
                ids(j)=flag2;
            end
            if j>=length(ids)-1 && flag1>0
                ids(j)=flag1;
            end
        end
    end
    if length(ids)<2
        ids=0;
    end
    sorted2(sorted2(:,1)==i,6)=ids;
end


sorted2=sortrows(sorted,1);
cnt=0;
for i=1:line_num
    tmp=sorted2(sorted2(:,1)==i,:);
    ids=tmp(:,5);
    
    cnt0=sum(ids==0);
    cnt1=sum(ids==1);
    cnt2=sum(ids==2);
   
    if length(ids)<3 || (cnt0>cnt1 && cnt0>cnt2)
        sorted2(sorted2(:,1)==i,6)=0;
    elseif cnt1>cnt0 && cnt1>cnt2
        sorted2(sorted2(:,1)==i,6)=1;
    else
        sorted2(sorted2(:,1)==i,6)=2;
    end 
end

conf=zeros(3,3);
conf(1,1)=sum(sorted(:,8)==0 & sorted(:,7)==0);
conf(1,2)=sum(sorted(:,8)==1 & sorted(:,7)==0);
conf(1,3)=sum(sorted(:,8)==2 & sorted(:,7)==0);
conf(2,1)=sum(sorted(:,8)==0 & sorted(:,7)==1);
conf(2,2)=sum(sorted(:,8)==1 & sorted(:,7)==1);
conf(2,3)=sum(sorted(:,8)==2 & sorted(:,7)==1);
conf(3,1)=sum(sorted(:,8)==0 & sorted(:,7)==2);
conf(3,2)=sum(sorted(:,8)==1 & sorted(:,7)==2);
conf(3,3)=sum(sorted(:,8)==2 & sorted(:,7)==2);
