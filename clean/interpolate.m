max_acc=max(R(:,1));
skipped=[];
for i=1:max_acc
    t=R(R(:,1)==i,:);
    a1=sum(t(1,3:end)==1);
    a2=sum(t(1,3:end)==2);
    for j=2:size(t,1)
        if sum(t(j,3:end)==1)~= a1 || sum(t(j,3:end)==2)~=a2
            skipped=[skipped;t(j,2)];
        end
    end
end


for i=1:size(skipped,1)
    for j=i:size(skipped,1)
        if skipped(j)-skipped(i)~=j-i
            break;
        end
    end
    if j-i>=3
        for j=i:size(skipped,1)
            skipped(j,2)=1;
            if skipped(j)-skipped(i)~=j-i
                break;
            end
        end
    end 
end
skipped(skipped(:,2)==1,:)=[];

f1=textread('F:\HERE\mergedLanes1_all4\chunk_1442_2_solid_leftb_3D.fuse');
f2=textread('F:\HERE\mergedLanes1_all4\chunk_1444_3_solid_leftb_3D.fuse');

f3=f1+repmat(f1(end,:),size(f1,1),1)-repmat(f1(1,:),size(f1,1),1);

fid3=fopen('F:\HERE\mergedLanes1_all4\chunk_1443_1_solid_leftb_3D_interpolated.fuse','w');
fprintf(fid3,'%.15f %.15f %.15f %.0f\n',f3');
fclose(fid3);

id=1442;
path2='F:\HERE\mergedLanes1_all4';
fuse_name=['chunk_',num2str(i),'_*.fuse'];
files=dir(fullfile(path2,fuse_name));
for i=1:length(files)
    f1=textread(fullfile(path2,files(i).name));
    f3=f1+repmat(f1(end,:),size(f1,1),1)-repmat(f1(1,:),size(f1,1),1);
    the_name='solid';
    ts=findstr(files(i).name,'solid');
    if isempty(ts)
        the_name='dashed';
        continue;
    end
    fuse_name2=['chunk_',num2str(id+1),'_',num2str(i),'_',the_name,'_interpolated.fuse'];
    fid3=fopen(fullfile(path2,fusename2),'w');
    fprintf(fid3,'%.15f %.15f %.15f %.0f\n',f3');
    fclose(fid3);
end